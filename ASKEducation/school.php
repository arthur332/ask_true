<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	if($data["get"] == "list") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		 
		 $data["type"] != "all"  
         ? $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? ORDER BY `name` ASC", $data["type"])
		 : $rs = $DB->Execute("SELECT * FROM `schools` ORDER BY `name` ASC");
		 
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "sort") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? ORDER BY ".$data['sortBy']." ASC", $data["type"]);
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "filter") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	     if($data['filterBy'] != "spec"){
             $data["filterValue"] != ""
             ? $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? AND ".$data['filterBy']." =? ORDER BY `name` ASC", array($data['type'], $data["filterValue"]))
             : $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? ORDER BY `name` ASC", $data["type"]);
         }else{
             if($data["filterValue"] != ""){
                 $rs = $DB->Execute("SELECT * FROM `linksspec` LEFT JOIN `schools` ON (`linksspec`.`schoolId`=`schools`.`id`) WHERE `specId` =?", $data["filterValue"]);
             }else{
                 $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? ORDER BY `name` ASC", $data["type"]);
             }
         }
		 echo json_encode(getFields($rs));
	}

    if($data["get"] == "filterMulty") {

        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

        $filters = array();

        if($data["filters"]["language"] != ""){
            $filters[] = "language = '".$data["filters"]["language"]."'";
        }
        if($data["filters"]["city"] != ""){
            $filters[] = "city = '".$data["filters"]["city"]."'";
        }
        if($data["filters"]["country"] != ""){
            $filters[] = "country = '".$data["filters"]["country"]."'";
        }
        if($data["filters"]["spec"] != ""){
            $filters[] = "specId = '".$data["filters"]["spec"]."'";
        }

        if(count($filters)){
            $filtersSQL = implode(" AND ", $filters);
            $filtersSQL = " AND ".$filtersSQL;
        }else{
            $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? ORDER BY `name` ASC", $data["type"]);
            echo json_encode(getFields($rs));
            return;
        }

        if($data["filters"]["spec"] != ""){
            $rs = $DB->Execute("SELECT * FROM `linksspec` LEFT JOIN `schools` ON (`linksspec`.`schoolId`=`schools`.`id`) WHERE type =? " .$filtersSQL,
                array(
                    $data["type"]
                )
            );
        }else{
            $rs = $DB->Execute("SELECT * FROM `schools` WHERE type =? " .$filtersSQL. " ORDER BY `name` ASC", $data['type']);
        }

        echo json_encode(getFields($rs));
    }
	
	if($data["get"] == "currencies") {
		$currencies = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
		
		if(!$currencies) {
			$done = false;
			$currencies	= simplexml_load_file("valute.xml");
		}else{
			$done = true;
		}
		
		$currenciesJson = json_encode($currencies);
		echo($currenciesJson);
		
		if($done == true){
			file_put_contents('valute.xml', file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp"));
		}
	}
	
	if($data["get"] == "school") {
		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		$school = $DB->Execute("SELECT * FROM `schools` WHERE id =? ", $data["id"]);

		echo json_encode(getFields($school));
		
	}
	
	if($data["get"] == "courses") {
		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
			
		$courses = $DB->Execute("SELECT * FROM `links` LEFT JOIN `courses` ON (`links`.`courseid`=`courses`.`id`) WHERE `schoolid` =?", $data["id"]);
		
		echo json_encode(getFields($courses));
		
	}
	
}

?>