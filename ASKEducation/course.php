<?php

if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	if($data["get"] == "list") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		 
		 $data["type"] != "all"  
         ? $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY `name` ASC", $data["type"])
		 : $rs = $DB->Execute("SELECT * FROM `courses` ORDER BY `name` ASC");
		 
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "sort") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY ".$data['sortBy']." ASC", $data["type"]);
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "filterMulty") {

		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

        $filters = array();

        if($data["filters"]["language"] != ""){
            $filters[] = "language = '".$data["filters"]["language"]."'";
        }
        if($data["filters"]["city"] != ""){
            $filters[] = "city = '".$data["filters"]["city"]."'";
        }
        if($data["filters"]["country"] != ""){
            $filters[] = "country = '".$data["filters"]["country"]."'";
        }
        if($data["filters"]["spec"] != ""){
            $filters[] = "spec = '".$data["filters"]["spec"]."'";
        }

        if(count($filters)){
            $filtersSQL = implode(" AND ", $filters);

            $filtersSQL = " AND ".$filtersSQL;
        }else{
            $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY `name` ASC", $data["type"]);
            echo json_encode(getFields($rs));
            return;
        }

         $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? " .$filtersSQL. " ORDER BY `name` ASC",
             array(
                 $data['type']
             )
         );

		 echo json_encode(getFields($rs));
	}

    /*if($data["get"] == "filter") {
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

        if($data["filterValue"] != ""){
            $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? AND ".$data['filterBy']." =? ORDER BY `name` ASC", array($data['type'], $data["filterValue"]));
        }else{
            $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY `name` ASC", $data["type"]);
        }

        echo json_encode(getFields($rs));
    }*/
	
	if($data["get"] == "currencies") {
		$currencies = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
		
		if(!$currencies) {
			$done = false;
			$currencies	= simplexml_load_file("valute.xml");
		}else{
			$done = true;
		}
		
		$currenciesJson = json_encode($currencies);
		echo($currenciesJson);
		
		if($done == true){
			file_put_contents('valute.xml', file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp"));
		}
	}
	
	if($data["get"] == "course") {
		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		$course = $DB->Execute("SELECT * FROM `courses` WHERE id =? ", $data["id"]);

		echo json_encode(getFields($course));
		
	}
	
	if($data["get"] == "schools") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
          
		 $rs = $DB->Execute("SELECT * FROM `links` LEFT JOIN `schools` ON (`links`.`schoolid`=`schools`.`id`) WHERE `courseid` =?", $data["id"]);
		 
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "reserve") {
		//$to="Sazonova_e@bk.ru";
		$to="xazy06@gmail.com";
		$to2="tonyzakharov@gmail.com";

		$subject = "Кто-то хочет получить предложение!";
		$date=date("l, F jS, Y");
		$time=date("h:i A");

		$name         = $data['name'];
		$phone        = $data['phone'];
		$mail         = $data['email'];
		$school       = $data['school'];
		$course       = $data['course'];

		$msg="Сообщение с сайта askeducation.ru от  $date, время: $time.\n	
		Имя: $name\n
		Телефон: $phone\n
		E-mail: $mail\n
		Школа: $school\n
		Курс: $course\n";

		mail($to,$subject,$msg,"From:askeducation.ru");
		mail($to2,$subject,$msg,"From:askeducation.ru");
		
	}
}

?>