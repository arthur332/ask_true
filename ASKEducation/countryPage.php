<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');

	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);

	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}

		return $temp;
    }
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET;
			break;
		case 'POST': $data  = &$_POST;
			break;
	}

	if($data["get"] == "currencies") {
		$currencies = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");

		if(!$currencies) {
			$done = false;
			$currencies	= simplexml_load_file("valute.xml");
		}else{
			$done = true;
		}

		$currenciesJson = json_encode($currencies);
		echo($currenciesJson);

		if($done == true){
			file_put_contents('valute.xml', file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp"));
		}
	}

	if($data["get"] == "sort") {
		$rs = $DB->Execute("SELECT * FROM `schools` WHERE `country`= ? ORDER BY ".$data['sortBy']." ASC", $data["type"]);

		echo json_encode(getFields($rs));
	}

	if($data["get"] == "filter") {
		$data["filterValue"] != ""
		 ? $rs = $DB->Execute("SELECT * FROM `schools` WHERE `country`= ? AND ".$data['filterBy']." =? ORDER BY `name` ASC", array($data["type"], $data["filterValue"]))
		 : $rs = $DB->Execute("SELECT * FROM `schools` WHERE `country`= ? ORDER BY `name` ASC", $data["type"]);


		echo json_encode(getFields($rs));
	}

    if($data["get"] == "filterMulty") {

        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

        $filters = array();

        if($data["filters"]["language"] != ""){
            $filters[] = "language = '".$data["filters"]["language"]."'";
        }
        if($data["filters"]["city"] != ""){
            $filters[] = "city = '".$data["filters"]["city"]."'";
        }
        if($data["filters"]["country"] != ""){
            $filters[] = "country = '".$data["filters"]["country"]."'";
        }
        if($data["filters"]["spec"] != ""){
            $filters[] = "specId = '".$data["filters"]["spec"]."'";
        }

        if(count($filters)){
            $filtersSQL = implode(" AND ", $filters);

            $filtersSQL = " AND ".$filtersSQL;
        }else{
            $rs = $DB->Execute("SELECT * FROM `schools` WHERE `country`= ? ORDER BY `name` ASC", $data["type"]);
            echo json_encode(getFields($rs));
            return;
        }

        if($data["filters"]["spec"] != ""){
            $rs = $DB->Execute("SELECT * FROM `linksspec` LEFT JOIN `schools` ON (`linksspec`.`schoolId`=`schools`.`id`) WHERE `country` =? " .$filtersSQL. " ",
                $data["type"]
            );
        }else{
            $rs = $DB->Execute("SELECT * FROM `schools` WHERE `country` =? " .$filtersSQL. " ORDER BY `name` ASC", $data['type']);
        }

        echo json_encode(getFields($rs));
    }


	if($data["get"] == "list") {
        $rs = $DB->Execute("SELECT * FROM `schools`  WHERE `country`= ? ORDER BY `name` ASC", $data["type"]);
		echo json_encode(getFields($rs));
	}

}

?>