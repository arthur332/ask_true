var AskApp = function() {
    var self = this;

    this.selectors = {
        $loader:$("#loader")
    };

    this.initScroll = function() {
        return $('html').niceScroll({
            zindex: "50002",
            scrollspeed: 50,
            mousescrollstep: 20,
            cursorwidth: 17,
            cursorborder: 0,
            background: "rgba(0,0,0,0.2)",
            cursorcolor: '#999999',
            cursorborderradius: 0,
            autohidemode: false,
            horizrailenabled: true
        });
    };

    this.topMenuFixer = function () {
        var $menu = $("#mainMenu"),
            settToTop = function () {
                
                var scrollTop = $(window).scrollTop();
                
                if (scrollTop > 122) {
                    $menu.addClass("fixed-top");
                }

                if (scrollTop < 122) {                    
                    $menu.removeClass("fixed-top");
                }
            };
        settToTop();

        $(window).on("scroll", function () {settToTop(); });
    };

    this.toggler = function () {
        var _this = this;
        this.$$selector = "a.js-toggler";

        $(document).on("click.repetitor", _this.$$selector, function () {
            $(this)
    		.toggleClass("open")
    		.parent()
    		.toggleClass("js-toggler_t");
        });
    };

    this.toggler_m = function () {
        var _this = this, $block = $("div.s-right-fixed");
        this.$$selector = "a.toggler_link_m";
        var clickHandler = function () {
            var top = $(window).scrollTop() + 82;
            $block.css({ top: top });
            $(this).toggleClass("open");
            $block.toggle();
        };

        $(document).on("click.repetitor", _this.$$selector, clickHandler);
    };

    this.toggler_any = function () {
        var _this = this;
        this.$$selector = "a.js_toggler";

        $(document).on("click.repetitor", _this.$$selector, function () {

            var $this = $(this),
    			toggleBlocks = $this.data("block"),
    			hideBlocks = $this.data("hide-blocks"),
    			showBlocks = $this.data("show-blocks"),
        		toggleClass = ($this.data("class") || "open");
            $this.toggleClass(toggleClass)
            $(toggleBlocks).slideToggle(200);
            $(hideBlocks).slideUp(200);
            $(showBlocks).slideDown(200);
        });
    };

    this.serializeObjectGetString = function (obj) {
        return _.map(obj, function (i, n) { return n + "=" + i; }).join("&");
    };

    this.serializeGetStringToObject = function (obj, splitResult, replaceQ) {
        var result = {};
        if (!obj) {
            return false;
        }

        if (replaceQ) {
            obj = obj.replace("?", "");
        }

        _.map(obj.split("&"), function (item) {
            var i = item.split("=");
            return result[i[0]] = i[1].indexOf(",") > -1 && splitResult
                ? i[1].split(",")
                : i[1];
        });
        return result;
    };

    this.initLoader = function() {
        $(document).on("ajaxStart", function() {
            self.selectors.$loader.show();
        }).on("ajaxStop", function () {
            self.selectors.$loader.hide();
        });
    };

    this.imgError = function(image) {
        $(image).attr("src", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAEsCAYAAAAfPc2WAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAQRElEQVR42uzd7XHiyBYG4DO3bgJsCEwInhCYEGxlgEPAIUAIdgayQ7BDWEIYQlhC8P1BU8P6ekwL9K3nqXKtd3eGj0OjfrvVan17f38PAADq8x8lAAAQsAAABCwAAAELAAABCwBAwAIAELAAABCwAAAELAAAAQsAAAELAEDAAgAQsAAABCwAAAQsAAABCwBAwAIAQMACABCwAAAELAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAABCwAAAQsAQMACABCwAAAQsAAABCwAAAELAAABCwBAwAIAELAAABCwAAAELAAAAQsAQMACAEDAAgAQsAAABCwAAAQsAAABCwBAwAIAQMACABCwAAAELAAABCwAAAELAEDAAgAQsAAAELAAAAQsAAABCwAAAQsAQMACABCwAAAQsAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAAELAAAAQsAQMACAEDAAgAQsAAABCwAAAQsAAABCwBAwAIAELAAABCwAAAELAAAAQsAAAELAEDAAgAQsAAAELAAAAQsAAABCwAAAQsAQMACABCwAAAELAAABCwAAAELAEDAAgBAwAIAELAAAAQsAAAELAAAAQsAQMACAEDAAgAQsAAABCwAAAELAAABCwBAwAIAELAAABCwAAAELACAMfivEgxbWZbX/PXVh99nGX/n4eT3XUS8DLBstxExT7/fpH/P8ZZ+hv7++W2R2kCk9r+q+B14ioj9pU9eFMWQjh+d1mqMuv78EbCozzIFi9UVj7H+5L/t0sHzJf3e1040N0R+9TiLP/y/p5M6XNOJ/DoJf5+5T8/Rpzb1eCaU/uzR671Jn+HyTJ1zvwPrD5//ZkTHiynVamjfOwQsemAehxmadcPPsU4/b2nUup3A+/4YNI6dyDYifmh6vTFLn8+1ATv3839KP1u1GnWt4EvWYI27U3lMI7N1i897ehqhC4uIeO7gfX+sPf0YXKwj4p/0z7Y+l2VE/B0Rrx1/F9QKBCwaOGj9czJSnEpn+pwO1LeawOStU8hedfgaFik8PPY8dKsVCFhkeI6v18SM0Sp1EIIVix6Ehc8GPL/iz+v3OlGWpVqBgEWGWRoBTilkzNN7Xvv4SUHhNS5fkN309/O1L221LEu1goZZ5D4el6xheInfi0u/uqLn5mREuejJ6PK41urS0wnHK5mOv+8z3/88pnXqdSierxhcbOP3dhtfXQk7O/nsby58vlVqQ3cdhiu1AgGLCp1Lbrg6HiCrXCK9/SSI3cbvS7i7mKm4ZHR7vLrx7YJO5fQqp/uTDqQvgXOqZqn9Ly74TKt+D/af/PlLvge3aUDU6vYVZVmqFbTIKcLhW2aODvcpGPyIevafeUmP9y3+vZlg38LV7uR1bi4IV+c6kJ8nNdhpjq17rRgYjvty1f09+Kvi4y3Sa1erftYKBKwpSyPSnLBx3JepqY3yNumg2fRGfFXD1X1EfI92NgjcpOe6C3v6tBkYcmdu9+mz+VljyP74+A+pDeQ+/qIsy8eWjhWDr1VM7+IdBCw6tIzza5B26UDZ9OzKcYbsLpq5HUaVcPXWUuD70yjdmpHmrSN/NuYtdeZt3Nbo+H3LndVdpgXnTYar0dQq+nXFIwhYI5ZzsGkq8HwVMOo+OFfZkf0hHbTd82y8bit0tJuO2sOmQtBel2XZyCabZVmOrlZhQ1IELBoeld7G+dmrPtyy5lrzyD81cBfjuhcc17WHh2h3feBng43cxdnPDRwj1AoELC6QM4p7GcH7zN2K4edI3i9fy72Ny0NPwvZbHE6dnw2O6VSeWuWFbHtkIWDRWcDaxvCvaltlBsn7aGYxLv1yG3lXzFbdUqBpT5mvZ5Vmna6WZrhHXavo5yapIGCNwLmDy24E7y9nlPoQ3Sxmp3057WEX/bzIIPd0/UqtWq8VCFhUClhDX+Sds3akb6NvmrOMvBmLPl/BmXP6a3ntLFZZlpOpVZjFQsCCSnJ2Rz/upcM05MxW9P2ijm3kzbau2qhVURRqBQIWF5gN+LXnHDQ3Yef0qbiNvBnbIZwqzplxXaZNhCtLa68mVauBH+sQsOihc+FiqPfHy5m92oVTg1MLWDmd8X4g39uccLNsslZFUagVCFh8MQr9yiyGuSFfbmfKNMwy2sRQZmSOcrYTqTxASrNeagUCFlfK2ZZgaCO7nA4id1TLOOS04acY1kUdb5ExA33BacKsWg1k9qpSrcJpQgQsapSzQHUZw5rFyjlQClfTMtYNdZuYmVErELCoaWSXMxJ9juFcypy7MSLTca7j3MYwbweV045v6q5Vz68cbLNWIGDxuTTNn3PgmQ8oZOV0pq4cnI6bOD+jOdQd/LcZA6TsWZl0s2i1AgGLmuQu9r6JiL97fhDK6SDMXk1LTnsd8s3Mz732mwrrsCZfq7AOCwGLuhRFsYv8zTZnEfEa/b1Jak4H4X6D0zIfeZvY1lQDtapWKxCwyLKpODJdRcSvyFvv1LfOdOvjnpScG5oP+ZZQuxpqkF2rgV092GStQMAi211UW5t0XJf1Gv05bZjTmTItY7+hec7rn6lV7bUCAYtKB5+fFxxEFylkPfdg9HeugxCwpmWW0WFOITScndlN67TUyilCBCwaDlmXBJHbOCyC7ypozSfQQVBv4I4Y9unB3DY9U6taawUCFhcfhH7E5VfbdRW0cg6Mex9vREQ8RsR7j34eO2wTYwjd+xrqoFYCFgIWLbmLiPsr/n7bQUvA4qP5RNrEXq1arRUIWFztKSK+x3V7Rx2D1mPDo8OpdBAACFiMwC4Os1lVrzL8aBkR/8RhiwcAQMAiDrNY3+OwKek1M0Hr6OceWgAgYNGZTUT8Ffm7v3/muIfWc1hUCgACFv8XtDZXPMZxfZbdkwFAwCLZx2Em65qgNU8hyynD9txHxLce/dx3WIspzKDu1Kr1WoGARW+C1nNctwA+Z12YXZqnZSobS85r+G6oVX6tQMCis6D1PQ5bPFS1viJk5RwYrfeaXnsce5uoa/83tRKwELAYyMzBfRx2hH+7IGTdXvicAhZVO8uhz2rOa/puqFV+rUDAonPbONzb8K7iyPDxgoO5U4Rc0lkO/QKLmzrqUBSFWglYCFgM0HEPrdwd4WdxWJNVNWCdOzgufBSTDPljDg1nBw1FUWzVKnuAtfWVQcBiaPZxmMnK3T/rJg67v9fZmc7CacKpGfvMzPzK70SlWpVlqVYgYNFTm8i/LL/qgvecztQs1rRsR94mzr32N7VqpFYgYNFLT5G3ncM8qs1i5XQQ9tualpxOc6ih4SbOz8ju1KqRWoGARW89NDBiHnMHwWW2cf4CiEUM89RxzmAhd93jca3W2VqVZTn5WoGAxRBCVp2BaB9567DMYk1LTvBeDvB9nWvH26Io9mqVV6uwBxYCFiPr+HICUZXtFXJGoUuln5QxtolFxvfiRa0arRUIWPQ+ZJ1Td8BahFOFU2tj+4w2NqSZzaZOeWXVqixLtQIBi56rewf2XWZoWyn9ZOwzO9ChtImciz/eMjcP/Zd0SnFytQoL3BGwELCy5M5iWYs1HTn3xrxk77UurGp6v1fVqixLtQIBiwmEsI8HzJy/sw4bj07FNvJmNvveJhYZIXBXFMXFp7zS1YRZter5FYVZtQqnBxGwGKmm7g+Yu8/Wo49gMnLaxKzHbSL3tW3UqtVagYDFYAPWJesjniJ/41HrsabhLfJmK/raJnJugr4tiuLqU15FUUyiVuH0IAIWI3buar6cmzj/Se59D9dh64apeIi8/Y7W0a81eqvM13OvVp3UCgQsDsqynJdl2fXIcx7nb7R7zf3B3iL/FMBjWPQ+BbsKbeI5+rGdxyqFmHM2af1ULdJViKOtVbi5MwIWDVqXZdnlGoqcA+G1B8GHyJ8Bew6nC6dgE/kLm187Dg65gWFbFMVD3U9eFMUoaxX5s9sgYHGxZUT86uDAuIzzM0b7qGeNxM/Ivw3GOgWtLt304DWM3X2F4P3aQfCepTaQExj2EXGnVr2oFQhY/Ms8HRhf4/wpuzosIu8Kn6eo5/5gu4oH1dsUOm87+BweI+Lvlj6HKTt2tFWC92s0d9XrNe3v7pJNRXOlzUdHU6uwqSgCFh1YpM79NZpb9P2YHj+nA6zzEuq3qLaodZ5GxW2c9lik5/oVFtu3aVsxeC/SZ9TUXlmL1N6eKzz+Xbrir1FpbdfgaxXXrekEAYtaDl6PEfEe9V1ht4qIfyo81n3Uf3f7p6h+5dDxQP53eg91dRbHy9vf0+NbYN+NtzicQr6kLa9rCt+rk4FNlce7jxY3yUxBTq2gRd/e399VYWDKspynEWbVEf/LScf0pwXopzfOvY3qp7s20ewi1Nu4bo3T6exazk7Qqw/PnVOPXUR8v/D1/YqvT8/cR7/2/1nG16eML+nYq7pJnfalAXqT2sW5dYOLk8//msD+M9UliqJo+9gx2Fo17Nz3rm0P6SIFBuy/SjAZN9H82qCnaP4Kn5eI+JFC1iUHxFnkLahlOLYnbeKSNn4aopu8Mve4nrCzLQaKotiWZalW0AKnCKnLJtrb/O/YoTptwGmH/CP6u7v3UxxmNTsPDEVR7IqiUCsQsPjEvmev5S7a35vm+LyuLOLUfRxOK/Wlc96l19O7nceLolArELD4cGDcpxFe1yPQTXodXc4kvaTXkHtrkDZq8lMr7dRbHGaz7jtsE/v0/N+j31e/qRUIWHwIWbs0Av0rqu14XoeHk+fty2zaJr2m+w5G5Mfdpb918FnwZ08nbaKtjvu4pchfMaybEasV1Mwi9+E7XhW3icMC7mX6Z907Mh+DQ9/XPT2ln+PVkItoZi+sp1SPujZUpfk2cfx+1N0m3tLPGNqCWkFNbNMwcGVZnvsjx8AVcbhq6NyeTS/xewaortvd9MHNSUcxj/w9vR4+hFkj7fG4pE1sToLBV9udZGl7m4YLjh+9qdUYdf35I2ABAAyKNVgAAAIWAICABQAgYAEAIGABAAhYAAACFgAAAhYAgIAFACBgAQAgYAEACFgAAAIWAICABQCAgAUAIGABAAhYAAAIWAAAAhYAgIAFAICABQAgYAEACFgAAAhYAAACFgCAgAUAIGABACBgAQAIWAAAAhYAAAIWAICABQAgYAEAIGABAAhYAAACFgAAAhYAgIAFACBgAQAIWAAACFgAAAIWAICABQCAgAUAIGABAAhYAAAIWAAAAhYAgIAFAICABQAgYAEACFgAAAIWAAACFgCAgAUAIGABACBgAQAIWAAAAhYAAAIWAICABQAgYAEACFgAAAhYAAACFgCAgAUAgIAFACBgAQAIWAAACFgAAAIWAICABQCAgAUAIGABAAhYAAACFgAAAhYAgIAFACBgAQAgYAEACFgAAAIWAAACFgCAgAUAIGABACBgAQAIWAAAAhYAgIAFAICABQAgYAEACFgAAAhYAAACFgCAgAUAgIAFACBgAQAIWAAACFgAAAIWAICABQAgYAEAIGABAAhYAAACFgAAAhYAgIAFACBgAQAgYAEACFgAAAIWAICABQCAgAUAIGABAAhYAAAIWAAAAhYAgIAFAICABQDQuv8NAAVW2btZQRVxAAAAAElFTkSuQmCC");
    };
    this.tabs = function() {
        +function($) {
            'use strict';

            // TAB CLASS DEFINITION
            // ====================

            var Tab = function(element) {
                this.element = $(element);
            }

            Tab.prototype.show = function() {
                var $this = this.element, $ul = $this.closest('ul:not(.dropdown-menu)'), selector = $this.data('target');

                if (!selector) {
                    selector = $this.attr('href');
                    selector = selector && selector.replace(/.*(?=#[^\s]*$)/, ''); //strip for ie7
                }

                if ($this.parent('li').hasClass('active')) {
                    return;
                }

                var previous = $ul.find('.active:last a')[0];
                var e = $.Event('show.bs.tab', {
                    relatedTarget: previous
                });

                $this.trigger(e);

                if (e.isDefaultPrevented()) {
                    return;
                }

                var $target = $(selector);

                this.activate($this.parent('li'), $ul);
                this.activate($target, $target.parent(), function() {
                    $this.trigger({
                        type: 'shown.bs.tab',
                        relatedTarget: previous
                    });
                });
            }

            Tab.prototype.activate = function(element, container, callback) {
                var $active = container.find('> .active');
                var transition = callback
                    && $.support.transition
                    && $active.hasClass('fade');

                function next() {
                    $active
                        .removeClass('active')
                        .find('> .dropdown-menu > .active')
                        .removeClass('active');

                    element.addClass('active');

                    if (transition) {
                        element[0].offsetWidth; // reflow for transition
                        element.addClass('in');
                    } else {
                        element.removeClass('fade');
                    }

                    if (element.parent('.dropdown-menu')) {
                        element.closest('li.dropdown').addClass('active');
                    }

                    callback && callback();
                }

                transition ?
                    $active
                    .one($.support.transition.end, next)
                    .emulateTransitionEnd(150) :
                    next();

                $active.removeClass('in');
            }


            // TAB PLUGIN DEFINITION
            // =====================

            var old = $.fn.tab;

            $.fn.tab = function(option) {
                return this.each(function() {
                    var $this = $(this);
                    var data = $this.data('bs.tab');

                    if (!data) {
                        $this.data('bs.tab', (data = new Tab(this)));
                    }
                    if (typeof option == 'string') {
                        data[option]();
                    }
                });
            }

            $.fn.tab.Constructor = Tab;


            // TAB NO CONFLICT
            // ===============

            $.fn.tab.noConflict = function() {
                $.fn.tab = old;
                return this;
            }


            // TAB DATA-API
            // ============

            $(document).on('click.bs.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function(e) {
                e.preventDefault();
                $(this).tab('show');
            });

        }(jQuery);

    };

   /**
    * @name {_interface}
    * @param {Object} setup
    * @param {Object} extra <as like beforeSend:function(){}..>
    * @return {Object}
    * @use /-self._interface({type:'get'|'post', url:url|+,(data:{})|undefined}, false|true)-/
    */
    this.ajaxInterface = function (setup, extra) {
        var _self = this;
        /**@type {Object} base*/
        this.base = {
            url: null,
            type: 'post',
            data: null
        };

        /**@set {url} */
        this.base.url = setup.url;

        !!setup.type && setup.type.toLowerCase() === "get"
            ? delete this.base.type
            : this.base.data = setup.data;

        !!extra
            ? $.extend(_self.base, extra)
            : void (0);

        return $.ajax(_self.base);
    };

    /**
     * @name {dispatchEvent}
     * @param {Event} eventName
     * @param {Object|int|String|array|undefined} data
     * @return {Object} self
     */
    this.dispatchEvent = function (eventName, data) {
        $(document).trigger(eventName, data);
        return this;
    };

    this.modal = function() {
        +function ($) {
            'use strict';

            // MODAL CLASS DEFINITION
            // ======================

            var Modal = function (element, options) {
                this.options = options
                this.$body = $(document.body)
                this.$element = $(element)
                this.$dialog = this.$element.find('.modal-dialog')
                this.$backdrop = null
                this.isShown = null
                this.originalBodyPad = null
                this.scrollbarWidth = 0
                this.ignoreBackdropClick = false

                if (this.options.remote) {
                    this.$element
                      .find('.modal-content')
                      .load(this.options.remote, $.proxy(function () {
                          this.$element.trigger('loaded.bs.modal')
                      }, this))
                }
            }

            Modal.VERSION = '3.3.4'

            Modal.TRANSITION_DURATION = 300
            Modal.BACKDROP_TRANSITION_DURATION = 150

            Modal.DEFAULTS = {
                backdrop: true,
                keyboard: true,
                show: true
            }

            Modal.prototype.toggle = function (_relatedTarget) {
                return this.isShown ? this.hide() : this.show(_relatedTarget)
            }

            Modal.prototype.show = function (_relatedTarget) {
                var that = this
                var e = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

                this.$element.trigger(e)

                if (this.isShown || e.isDefaultPrevented()) return

                this.isShown = true

                this.checkScrollbar()
                this.setScrollbar()
                this.$body.addClass('modal-open')

                this.escape()
                this.resize()

                this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

                this.$dialog.on('mousedown.dismiss.bs.modal', function () {
                    that.$element.one('mouseup.dismiss.bs.modal', function (e) {
                        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
                    })
                })

                this.backdrop(function () {
                    var transition = $.support.transition && that.$element.hasClass('fade')

                    if (!that.$element.parent().length) {
                        that.$element.appendTo(that.$body) // don't move modals dom position
                    }

                    that.$element
                      .show()
                      .scrollTop(0)

                    that.adjustDialog()

                    if (transition) {
                        that.$element[0].offsetWidth // force reflow
                    }

                    that.$element
                      .addClass('in')
                      .attr('aria-hidden', false)

                    that.enforceFocus()

                    var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

                    transition ?
                      that.$dialog // wait for modal to slide in
                        .one('bsTransitionEnd', function () {
                            that.$element.trigger('focus').trigger(e)
                        })
                        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
                      that.$element.trigger('focus').trigger(e)
                })
            }

            Modal.prototype.hide = function (e) {
                if (e) e.preventDefault()

                e = $.Event('hide.bs.modal')

                this.$element.trigger(e)

                if (!this.isShown || e.isDefaultPrevented()) return

                this.isShown = false

                this.escape()
                this.resize()

                $(document).off('focusin.bs.modal')

                this.$element
                  .removeClass('in')
                  .attr('aria-hidden', true)
                  .off('click.dismiss.bs.modal')
                  .off('mouseup.dismiss.bs.modal')

                this.$dialog.off('mousedown.dismiss.bs.modal')

                $.support.transition && this.$element.hasClass('fade') ?
                  this.$element
                    .one('bsTransitionEnd', $.proxy(this.hideModal, this))
                    .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
                  this.hideModal()
            }

            Modal.prototype.enforceFocus = function () {
                $(document)
                  .off('focusin.bs.modal') // guard against infinite focus loop
                  .on('focusin.bs.modal', $.proxy(function (e) {
                      if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
                          this.$element.trigger('focus')
                      }
                  }, this))
            }

            Modal.prototype.escape = function () {
                if (this.isShown && this.options.keyboard) {
                    this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
                        e.which == 27 && this.hide()
                    }, this))
                } else if (!this.isShown) {
                    this.$element.off('keydown.dismiss.bs.modal')
                }
            }

            Modal.prototype.resize = function () {
                if (this.isShown) {
                    $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
                } else {
                    $(window).off('resize.bs.modal')
                }
            }

            Modal.prototype.hideModal = function () {
                var that = this
                this.$element.hide()
                this.backdrop(function () {
                    that.$body.removeClass('modal-open')
                    that.resetAdjustments()
                    that.resetScrollbar()
                    that.$element.trigger('hidden.bs.modal')
                })
            }

            Modal.prototype.removeBackdrop = function () {
                this.$backdrop && this.$backdrop.remove()
                this.$backdrop = null
            }

            Modal.prototype.backdrop = function (callback) {
                var that = this
                var animate = this.$element.hasClass('fade') ? 'fade' : ''

                if (this.isShown && this.options.backdrop) {
                    var doAnimate = $.support.transition && animate

                    this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
                      .appendTo(this.$body)

                    this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
                        if (this.ignoreBackdropClick) {
                            this.ignoreBackdropClick = false
                            return
                        }
                        if (e.target !== e.currentTarget) return
                        this.options.backdrop == 'static'
                          ? this.$element[0].focus()
                          : this.hide()
                    }, this))

                    if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

                    this.$backdrop.addClass('in')

                    if (!callback) return

                    doAnimate ?
                      this.$backdrop
                        .one('bsTransitionEnd', callback)
                        .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
                      callback()

                } else if (!this.isShown && this.$backdrop) {
                    this.$backdrop.removeClass('in')

                    var callbackRemove = function () {
                        that.removeBackdrop()
                        callback && callback()
                    }
                    $.support.transition && this.$element.hasClass('fade') ?
                      this.$backdrop
                        .one('bsTransitionEnd', callbackRemove)
                        .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
                      callbackRemove()

                } else if (callback) {
                    callback()
                }
            }

            // these following methods are used to handle overflowing modals

            Modal.prototype.handleUpdate = function () {
                this.adjustDialog()
            }

            Modal.prototype.adjustDialog = function () {
                var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

                this.$element.css({
                    paddingLeft: !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
                    paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
                })
            }

            Modal.prototype.resetAdjustments = function () {
                this.$element.css({
                    paddingLeft: '',
                    paddingRight: ''
                })
            }

            Modal.prototype.checkScrollbar = function () {
                var fullWindowWidth = window.innerWidth
                if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
                    var documentElementRect = document.documentElement.getBoundingClientRect()
                    fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
                }
                this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
                this.scrollbarWidth = this.measureScrollbar()
            }

            Modal.prototype.setScrollbar = function () {
                var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
                this.originalBodyPad = document.body.style.paddingRight || ''
                if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
            }

            Modal.prototype.resetScrollbar = function () {
                this.$body.css('padding-right', this.originalBodyPad)
            }

            Modal.prototype.measureScrollbar = function () { // thx walsh
                var scrollDiv = document.createElement('div')
                scrollDiv.className = 'modal-scrollbar-measure'
                this.$body.append(scrollDiv)
                var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
                this.$body[0].removeChild(scrollDiv)
                return scrollbarWidth
            }


            // MODAL PLUGIN DEFINITION
            // =======================

            function Plugin(option, _relatedTarget) {
                return this.each(function () {
                    var $this = $(this)
                    var data = $this.data('bs.modal')
                    var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

                    if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
                    if (typeof option == 'string') data[option](_relatedTarget)
                    else if (options.show) data.show(_relatedTarget)
                })
            }

            var old = $.fn.modal

            $.fn.modal = Plugin
            $.fn.modal.Constructor = Modal


            // MODAL NO CONFLICT
            // =================

            $.fn.modal.noConflict = function () {
                $.fn.modal = old
                return this
            }


            // MODAL DATA-API
            // ==============

            $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
                var $this = $(this)
                var href = $this.attr('href')
                var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
                var option = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

                if ($this.is('a')) e.preventDefault()

                $target.one('show.bs.modal', function (showEvent) {
                    if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
                    $target.one('hidden.bs.modal', function () {
                        $this.is(':visible') && $this.trigger('focus')
                    })
                })
                Plugin.call($target, option, this)
            })

        }(jQuery);

    };

    this.initMobileMenu = function() {
        var $opener = $("#open-left"), $menu = $("#mobile-menu");
        $opener.on("click", function() {
            $menu.toggle();
        });
        $(document).on("click.menu", function(e) {
            if (!$(e.target).closest("#mobile-menu, #open-left").length) {
                $menu.hide();
            }

        });

    };

    this.init = function () {
        //askApp.initScroll();
        //askApp.initLoader();
        askApp.toggler();
        askApp.initMobileMenu();
        askApp.topMenuFixer();
    };

};

var askApp = new AskApp();
askApp.init();



;(function ($, window, document) {
    /**
	 * Alert Plagin
	 *
	 * Dual licensed under the MIT and GPL licenses:
	 * http://www.opensource.org/licenses/mit-license.php
	 * http://www.gnu.org/licenses/gpl.html
	 *
	 * @author Arthur Enokyan/xazy06@gmail.com
	 *
	 *  -------EXAMPLE-------
	 *
	 *   $.alert('alert text'); // renders simple alert as default[red color]
	 *   $.alert(['alert text1', 'alert text2'], 'success'); // renders success alert with multi string content
	 *   $.alert('alert text', 'info'); //renders simple info alert
	 *   
	 *
	 */
    $.alert = function (alertText, Atype, autohide) {
        if (alertText instanceof Array) {
            for (var i = 0, len = alertText.length, spanMessages = ''; i < len ; ++i) {
                spanMessages += '<span>' + alertText[i] + '</span><br/>';
            }
        } else {
            var spanMessages = alertText;
        }

        Atype === undefined
		? Atype = 'error'
		: Atype = Atype
        ;
        /**
         *   Main template {Object JQuery}
         */
        var $template = ('<div class="alert-block_' + Atype + ' alert alert-block">' +
						 '    <i class="icon icon-alert_big___' + Atype + ' s-mr_20"></i>' +
						 '    <span class="alert-heading">' + spanMessages + '</span>' +
						 '    <a class="close icon s-float_r s-pt_31" data-dismiss="alert" href="javascript:void(0)">' +
						 '        <i class="icon icon-alert_close"></i>' +
						 '    </a>' +
						 '</div>')
		  , appPlace = $('body')
          , closer = $('[data-dismiss="alert"]')
          , aBlock = $('.alert-block')
        ;
        aBlock.remove();
        switch (Atype) {
            //*/ if needed difference
            case 'error': appPlace.append($template);
                break;

            case 'info': appPlace.append($template);
                break;

            case 'warning': appPlace.append($template);
                break;

            case 'success': appPlace.append($template);
                break;
                //*/	
            default: appPlace.append($template);
                break;
        }
        if (!autohide) {
            setTimeout(function () {
                $('.alert').fadeOut(100);
            }, 2000);
        }
        $(document).on('click', '[data-dismiss="alert"]', function (event) {
            event = event || window.event;
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            $(this).parent().remove();
        });
    };
})(jQuery, window, document);