﻿/**
 * @name {AboutController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var AboutController = function (controller) {
    var self = this;

    this.dataGot = 0;
    this.dataNeed = 1;

    this.getData = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "instanse" } }, false).done(self.getDataCallback);
    };

    this.getDataCallback = function (response) {
        self.data = JSON.parse(response)[0];
        
        self.dataGot++;
        askApp.dispatchEvent("aboutGot");

        console.log(self.data);
    };

    this.init = function () {
        self.dataGot = 0;
        self.getData();
    };
};

var AC = new AboutController("../about");
AC.init();

/**
 * @name {aboutModel}
 * @return {undefined}
 */
var AboutModel = function () {
    var self = this;

    this.data = ko.mapping.fromJS(AC.data);

    this.methods = {
        send: "/feedback.php"
    };

    this.name = ko.observable("");
    this.secondName = ko.observable("");
    this.phone = ko.observable("");
    this.mail = ko.observable("");
    this.message = ko.observable("");

    this.yandexCounderBind = function () {
        var date = new Date(),_this = this,capcha,fd = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' , ' + (date.getHours()) + ':' + (date.getMinutes() + 1);

        this.params = {
            fd: fd,
            captcha: 'request_' + fd,
            yaParams: {}
        };

        this.params.yaParams[_this.params.captcha] = {
            'имя': self.name(),
            'телефон': self.phone(),
            'дата записи': fd,
            'сообщение': self.message()
        }
        
        return yaCounter29767232.reachGoal('send_message', _this.params.yaParams);
    };

    this.submit = function () {
        self.mess = {
            name: self.name(),
            secondName: self.secondName(),
            phone: self.phone(),
            mail: self.mail(),
            sub: "Форма обратной связи",
            mess: self.message()
        };

        self.yandexCounderBind();

        if (!!self.name().length && !!self.phone().length && !!self.mail().length && !!self.mail().indexOf("@") && self.enable()) {
            askApp.ajaxInterface({ type: "post", url: self.methods.send, data: self.mess }, false).done(self.submitCallback);
        } else {
            $.alert("Пожалуйста заполните все поля!");
        }

    };

    this.submitCallback = function (response) {
        $.alert("Мы скоро с Вами свяжемся, спасибо!", "success");
        self.name("");
        self.secondName("");
        self.phone("");
        self.mail("");
        self.message("");
        self.capchaInputResult("");
    };

    this.capcha1 = ko.observable(parseFloat(Math.random() * 10).toFixed(0));
    this.capcha2 = ko.observable(parseFloat(Math.random() * 10).toFixed(0));
    this.capchaTrueResult = ko.observable(+this.capcha1() + +this.capcha2());

    this.capchaInputResult = ko.observable();

    this.enable = function () {
        return self.capchaInputResult() == self.capchaTrueResult();
    };
    
};

$(document).on("aboutGot", function () {
    
    if (AC.dataNeed === AC.dataGot) {
        var AM = new AboutModel();

        ko.applyBindings(AM);
        window.am = AM;
    }
});
    
