﻿/**
 * @name {BlogController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var BlogController = function (controller) {
    var self = this;

    this.dataGot = 0;
    this.dataNeed = 0;

    this.getBlog = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "list" } }, false).done(self.getBlogCallback);
    };

    this.modelFiller = function (source, target) {
        for (var i in source) {
            if (source.hasOwnProperty(i)) {
                target.push(source[i]);
            }
        }
    };

    this.getBlogCallback = function (response) {
        self.blog = JSON.parse(response);
        self.modelFiller(self.blog, BM.blog);
    };

    this.init = function () {
        self.dataGot = 0;
        self.getBlog();
    };
};

var BC = new BlogController("blog").init();

/**
 * @name {BlogModel}
 * @return {undefined}
 */
var BlogModel = function () {
    var self = this;

    this.blog = ko.observableArray([]);

};

var BM = new BlogModel();

ko.applyBindings(BM);
