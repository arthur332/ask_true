﻿/**
 * @name {SchoolController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var SchoolController = function () {
    var self = this;

    this.dataGot = 0;

    this.dataNeed = 4;

    this.selectors = {
        modal: "#reserveCourse"
    };

    this.methods = {
        nzd: "https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22NZDRUB%22&format=json&env=store://datatables.org/alltableswithkeys&callback="
    };

    this.strings = {
        successSend: "Мы свяжемся с Вами в ближайшее время, спасибо!",
        unsuccessSend: "Пожалуйста укажите ваши данные иначе мы не сможем с Вами связаться!",
        error : "Ошибка, попробуйте еще раз!"
    }

    this.getData = function () {
        askApp.ajaxInterface({ url: self.controller.concat(".php"), data: { get: "school", id:self.id } }, false).done(self.getDataCallback);
    };

    this.getCourses = function () {
        askApp.ajaxInterface({ url: self.controller.concat(".php"), data: { get: "courses", id: self.id } }, false)
            .done(self.getCoursesCallback);
    };

    this.getNZD = function () {
        askApp.ajaxInterface({ url: self.methods.nzd }, false)
            .done(self.getNZDCallback)
            .fail(self.getNZDErrorCallback);
    };

    this.getNZDCallback = function (response) {
        self.nzd = response;

        self.dataGot++;
        askApp.dispatchEvent("dataGot");

        self.getCurrencies();
    };

    this.getNZDErrorCallback = function () {
        self.nzd = {};

        self.dataGot++;
        askApp.dispatchEvent("dataGot");

        self.getCurrencies();
    };


    this.getCurrencies = function () {
        askApp.ajaxInterface({ url: self.controller.concat(".php"), data: { get: "currencies" } }, false).done(self.getCurrenciesCallback);
    };

    this.getCurrenciesCallback = function (response) {

        self.currencies = self.mergeCurrencies(JSON.parse(response), self.nzd);

        self.cCurrencies = _.indexBy(self.currencies.Valute, "NumCode");

        self.dataGot++;
        askApp.dispatchEvent("schoolGot");


        //self.getData();
        
    };

    this.mergeCurrencies = function (all, nzd) {
        all.Valute.push({
            CharCode: "NZD",
            Name: "Новозеландский доллар",
            Nominal: "1",
            NumCode: "554",
            //Value: "query" in nzd ? nzd.query.results.rate.Rate : "51.7355"
            Value: "51.7355"
        });

        return all;
    };

    this.getCoursesCallback = function (response) {
        self.courses = JSON.parse(response);
        self.coursesGroup = _.groupBy(self.courses, 'type');
        

        self.dataGot++;
        askApp.dispatchEvent("schoolGot");

    };

    this.getDataCallback = function (response) {
        self.data = JSON.parse(response)[0];
        
        self.dataGot++;
        askApp.dispatchEvent("schoolGot");

    };

    this.sendCallback = function (response) {
        /*if (!response) {
            $.alert(self.strings.error);
            return false;
        }*/
        $(self.selectors.modal).modal("hide");
        $.alert(self.strings.successSend, "success");
    };

    this.init = function (id, controller) {
        self.controller = controller;
        self.id = id;
        self.dataGot = 0;
        self.getData();
        self.getCourses();
        self.getNZD();
    };
};

var SC = new SchoolController();

/**
 * @name {SchoolModel}
 * @return {undefined}
 */
var SchoolModel = function () {
    var self = this;

    this.generalData = ko.mapping.fromJS(SC.data);
    

    this.currencyValue = ko.observable("");

    this.currencyCode = ko.observable("643");

    this.currencyIconValue = ko.observable("Р");

    this.currencyIcon = function () {
        switch (self.currencyCode()) {
            case 643: {
                self.currencyIconValue("Р");
                break;
            }
            case 840: {
                self.currencyIconValue("USD");
                break;
            }
            case 978: {
                self.currencyIconValue("EUR");
                break;
            }
            case 826: {
                self.currencyIconValue("GBP");
                break;
            }
            case 124: {
                self.currencyIconValue("CAD");
                break;
            }
            case "036": {
                self.currencyIconValue("AUD");
                break;
            }
            case 756: {
                self.currencyIconValue("CHF");
                break;
            }
            case 554: {
                self.currencyIconValue("NZD");
                break;
            }
            case 156: {
                self.currencyIconValue("CNY");
                break;
            }
        }
    };

    this.changeCurrency = function (data, event) {
        var code = $(event.currentTarget || event.srcElement).data("code");
        if (code == "643") {
            self.currencyValue(false);

        } else {
            self.currencyValue($.grep(SC.currencies.Valute, function (item) {
                return item.NumCode == code;
            })[0].Value.replace(",", "."));
        }

        self.currencyCode(code);

        self.currencyIcon();

        console.log(self.currencyValue());

    };

    this.showBig = function (item, event) {
        $("#preview").attr("src", $(event.target || event.srcElement).closest("a").data("url"));
        self.visiblePreview(true);
    };
    this.visiblePreview = ko.observable(false);

    this.hidePreview = function() {
        self.visiblePreview(false);
    };
    this.showDataWhhenReserving = ko.observable("");

    this.reserve = function () {
        var id = this.id();
        self.showDataWhhenReserving(
        $.grep(SC.courses, function(item) {
            return item.id === id;
        })[0]);
        console.log(self.showDataWhhenReserving());
    };

    this.sendName = ko.observable("");
    this.sendSecondName = ko.observable("");
    this.sendPhone = ko.observable("");
    this.sendEmail = ko.observable("");
    this.sendMessage = ko.observable("");

    this.yandexCounderBind = function () {
        var date = new Date(), _this = this, capcha, fd = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' , ' + (date.getHours()) + ':' + (date.getMinutes() + 1);

        this.params = {
            fd: fd,
            captcha: 'request_' + fd,
            yaParams: {}
        };

        this.params.yaParams[_this.params.captcha] = {
            'имя': self.sendName(),
            'телефон': self.sendPhone() + " " + self.sendSecondName(),
            'дата записи': fd,
            'название': self.showDataWhhenReserving().name
        }

        return yaCounter29767232.reachGoal('reservation', _this.params.yaParams);
    };

    this.send = function () {
        if (self.sendName().length === 0 || self.sendPhone().length === 0 || self.sendEmail().length === 0) {
            $.alert(SC.strings.unsuccessSend);
            return false;
        }

        self.yandexCounderBind();

        askApp.ajaxInterface({
            type: "post", url: "/reservation.php", data: {
            sendName: self.sendName(),
            sendSecondName: self.sendSecondName(),
            sendPhone: self.sendPhone(),
            sendEmail: self.sendEmail(),
            sendMesage: self.sendMessage(),
            id: self.showDataWhhenReserving().id,
            courseName: self.showDataWhhenReserving().name,
            cost: self.showDataWhhenReserving().cost,
            currency: self.currencyIconValue(),
            sub: "Бронирование"
        } }, false).done(SC.sendCallback);
    };

    this.hack = ko.observable(false);

};

$(document).bind("keydown", function (a) {
    a = a || window.event;
    1 == a.altKey && (1 == a.shiftKey && 1 == a.ctrlKey) && (sm.hack(!!sm.hack() ? !1 : !0));
});

$(document).on("schoolGot", function () {
    
    if (SC.dataNeed === SC.dataGot) {
        
        var SM = new SchoolModel(), $costTab = $("#costTab");

        SM.currencyValue(SC.cCurrencies[SM.currencyCode()]);
        for(var i in SC.coursesGroup){
            (function (i) {
                ko.utils.arrayForEach(SC.coursesGroup[i], function (subItem, n) {
                    (function (n) {
                        subItem.cost = ko.computed(function () {
                            //return !SM.currencyValue() ? subItem.costCalc : parseFloat(subItem.costCalc / SM.currencyValue()).toFixed(0);
                            if (SM.currencyCode() == "643") {
                                if (SM.currencyCode() == subItem.costCurrency) {
                                    return subItem.costCalc;
                                } else {
                                    return parseFloat(subItem.costCalc * SC.cCurrencies[subItem.costCurrency].Value.replace(",", ".")).toFixed(0);
                                }
                            } else {
                                if (subItem.costCurrency == "643") {
                                    return parseFloat(subItem.costCalc / FM.currencyValue()).toFixed(0);
                                } else {
                                    return parseFloat(subItem.costCalc * SC.cCurrencies[subItem.costCurrency].Value.replace(",", ".") / SM.currencyValue()).toFixed(0);
                                }
                            }

                        }, SM);
                    })(n);
                });
            })(i);
        }
        
        SM.courses = ko.mapping.fromJS(SC.coursesGroup);

        ko.applyBindings(SM);
       
        
        window.sm = SM;

        if (document.location.hash.slice(1, 7) === "course") {

            $costTab.tab("show");

            $(window).scrollTop($('[name="' + document.location.hash.slice(8) + '"]').offset().top);
        }
    }
});
