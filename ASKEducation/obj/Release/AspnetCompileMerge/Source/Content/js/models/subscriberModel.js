﻿var SubscribeController = function() {
    var self = this;

    this.methods = {
        subscribe:"/subscribe"    
    };
    this.strings = {
        success: "Спасибо за подписку, теперь Вы будете в курсе всех наших акций и специальных предложений!",
        dublicate: "Вы уже подписывались раннее, извините!",
        incompete: "Заполните все поля, пожалуйста!",
        error:"Извините, произошла ошибка, попробуйте еще раз!"
    };
    

    this.subscribe = function(name, email) {
        askApp.ajaxInterface({ url: self.methods.subscribe.concat(".php"), data: { name:name, email:email } }, false).done(self.subscribeCallback);
    };

    this.subscribeCallback = function (response) {
        console.log(response);
        console.log(JSON.parse(response));
        switch (JSON.parse(response)) {
            case true:
                $.alert(self.strings.success, "success");
                model.closeBlock();
                model.$name.val("");
                model.$email.val("");
                break;
            case false:
                $.alert(self.strings.error, "error");
                break;
            case "dublicate":
                $.alert(self.strings.dublicate, "warning");
                break;
        }
    };


    this.init = function() {};

}, controller, SubscribeModel, model;

controller = new SubscribeController();

controller.init();

SubscribeModel = function() {
    var self = this;

    this.$name = $("#name");
    this.$email = $("#email");

    this.checker = function() {
        return !!self.$name.val().length && !!self.$email.val().match(/@/);
    };

    this.submit = function () {
        self.checker() ? controller.subscribe(self.$name.val(), self.$email.val()) : $.alert(controller.strings.incompete);
    };

    this.closeBlock = function () {
        console.log("close");
        $("#subscribe").hide();
        window.localStorage.setItem("subscribe", "no");
    };

};

model = new SubscribeModel();

$(document).on("click","#subscribeSubmit", function() {
    model.submit();
}).on("click", "#subscribeCloser", function () {
    model.closeBlock();
});
