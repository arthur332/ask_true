﻿/**
 * @name {CooperationModel}
 * @return {undefined}
 */
var CooperationModel = function () {
    var self = this;

    this.methods = {
        send: "/feedbackCooperation.php"
    };

    this.name = ko.observable("");
    this.secondName = ko.observable("");
    this.phone = ko.observable("");
    this.message = ko.observable("");
    this.time = ko.observable("");
    this.email = ko.observable("");
    this.accept = ko.observable(false);

    this.send = function () {
        self.mail = {
            name: self.name(),
            secondName: self.secondName(),
            phone: self.phone(),
            time: self.time(),
            email: self.email(),
            sub: "Групповые поездки",
            mess: self.message()
        };

        if (!!self.name().length && !!self.phone().length && !!self.email().length && !!self.email().match(/@/) && !!self.accept() && self.enable()) {
            askApp.ajaxInterface({ type: "post", url: self.methods.send, data: self.mail }, false).done(self.submitCallback);
        } else {
            $.alert("Заполните обязательные поля!");
        }
    };

    this.submitCallback = function (response) {
        $.alert("Заявка успешно отправлена.", "success");
        self.name("");
        self.secondName("");
        self.phone("");
        self.time("");
        self.message("");
        self.email("");
        self.accept(false);
    };

    this.capcha1 = ko.observable(parseFloat(Math.random() * 10).toFixed(0));
    this.capcha2 = ko.observable(parseFloat(Math.random() * 10).toFixed(0));
    this.capchaTrueResult = ko.observable(+this.capcha1() + +this.capcha2());

    this.capchaInputResult = ko.observable();

    this.enable = function () {
        return self.capchaInputResult() == self.capchaTrueResult();
    };

};

var AM = new CooperationModel();
ko.applyBindings(AM);

