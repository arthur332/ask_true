﻿/**
* @name {ReviewController}
* @author {Enokyan A/R}
* @return {Object}
*/
var ReviewController = function (controller) {
    var self = this;

    this.dataGot = 0;
    this.dataNeed = 0;

    this.getReview = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "list" } }, false).done(self.getReviewCallback);
    };


    this.getReviewCallback = function (response) {
        self.review = JSON.parse(response);

        ko.utils.arrayForEach(self.review, function (item) {
            item.complete = ko.computed(function () {
                return parseFloat((+item.study + +item.infra + +item.activ + +item.cost_q) / 4).toFixed(1);
            }, RM);
        });
        RM.review(self.review);
    };

    this.init = function () {
        self.dataGot = 0;
        self.getReview();
    };
};

var BC = new ReviewController("review").init();

/**
 * @name {ReviewModel}
 * @return {undefined}
 */
var ReviewModel = function () {
    var self = this;

    this.review = ko.observableArray();

};

var RM = new ReviewModel();

ko.applyBindings(RM);
