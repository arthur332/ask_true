﻿/**
 * @name {BlogController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var CountryPageController = function (controller) {
    var self = this;

    this.dataGot = 0;
    this.dataNeed = 1;

    this.getCountry = function (id) {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "country", id: id } }, false).done(self.getCountryCallback);
    };

    this.getCountryCallback = function (response) {
        self.country = JSON.parse(response)[0];

        self.dataGot++;
        askApp.dispatchEvent("countryGot");

        console.log(self.country);
    };

    this.init = function (id) {
        self.id = id;
        self.dataGot = 0;
        self.getCountry(id);
    };
};

var CC = new CountryPageController("../../country");

/**
 * @name {BlogModel}
 * @return {undefined}
 */
var CountryPageModel = function () {
    var self = this;

    this.country = ko.mapping.fromJS(CC.blog);

};

$(document).on("countryGot", function () {

    if (BC.dataNeed === BC.dataGot) {
        var CM = new CountryPageModel();

        ko.applyBindings(CM);
        window.cm = CM;
    }
});

