﻿/**
 * @name {BlogController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var BlogPageController = function (controller) {
    var self = this;

    this.dataGot = 0;
    this.dataNeed = 1;

    this.getBlog = function (id) {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "blog", id:id } }, false).done(self.getBlogCallback);
    };

    this.getBlogCallback = function (response) {
        self.blog = JSON.parse(response)[0];
        
        self.dataGot++;
        askApp.dispatchEvent("blogGot");

        console.log(self.blog);
    };

    this.init = function (id) {
        self.id = id;
        self.dataGot = 0;
        self.getBlog(id);
    };
};

var BC = new BlogPageController("../../blog");

/**
 * @name {BlogModel}
 * @return {undefined}
 */
var BlogPageModel = function () {
    var self = this;

    this.blog = ko.mapping.fromJS(BC.blog);

};

$(document).on("blogGot", function () {
    
    if (BC.dataNeed === BC.dataGot) {
        var BM = new BlogPageModel();

        ko.applyBindings(BM);
        window.bm = BM;
    }
});
    
