﻿var programmController,
    ViewModel,
    model,
    FindProgrammController = function () {
        var self = this;

        this.strings = {};

        this.methods = {
            send: "/program.php"//ask@askeducation.ru
        };

        this.selectors = {
            "js-datepicker": ".js-datepicker"
        };

        this.sendData = function(data) {
            askApp.ajaxInterface({ url: self.methods.send, data: data }, false)
                .done(self.sendDataCallback);
        };

        this.sendDataCallback = function(response) {
            if (response === 1) {
                $.alert("Успешно отправлено");
            }
        };


        this.initDatepicker = function() {
            $(self.selectors["js-datepicker"]).datepicker({
                format: 'dd.mm.yyyy',
                autoclose: true,
                language:"ru"
            });
        };


        this.init = function () {

            var model = new ViewModel();

            ko.applyBindings(model, document.getElementById("view-model"));

            self.initDatepicker();
        };

};

programmController = new FindProgrammController();

ViewModel = function() {
    var self = this;

    this.birthDate = ko.observable("");

    this.studyType = ko.observable("Языковые курсы");

    this.studyLanguage = ko.observable("Английский");

    this.studyCountry = ko.observable("Великобритания");

    this.studyStartDate = ko.observable("");

    this.studyLength = ko.observable("");

    this.referrer = ko.observable("Яндекс");

    this.userName = ko.observable("");

    this.email = ko.observable("");

    this.phone = ko.observable("");

    this.additionalInfo = ko.observable("");

    this.isValid = function() {
        return !!self.birthDate().length && !!self.studyStartDate().length && !!self.studyLength().length && !!self.userName().length;
    };

    this.send = function () {

        var model = ko.toJS(self);

        if (!self.isValid()) {
            $.alert("Заполните поля, отмеченные звездочкой", "warning");
            return;
        }

        delete model.isValid;
        delete model.send;

        programmController.sendData(model);
    };
};

programmController.init();