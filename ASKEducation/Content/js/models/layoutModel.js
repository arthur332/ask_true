﻿/**
 * @name {FilterController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var LayoutController = function (controller) {
    var self = this;

    this.dataGot = 0;
    this.dataNeed = 0;

    this.getBestSchools = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "schoolsWhithCourses"} }, false).done(self.getBestSchoolsCallback);
    };

    this.search = function (age, language, type) {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "searchCourses", age: age, type: type, language: language } }, false).done(self.searchCallback);
    };

    this.getBestCourses = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "coursesWhithSchools" } }, false).done(self.getBestCoursesCallback);
    };

    this.getReviews = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "reviews" } }, false).done(self.getReviewsCallback);
    };

    this.getBlog = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "blog" } }, false).done(self.getBlogCallback);
    };

    this.modelFiller = function(source, target) {
        for (var i in source) {
            if (source.hasOwnProperty(i)) {
                target.push(source[i]);
            }
        }
    };

    this.searchCallback = function (response) {
        LM.emptySearch("");
        self.searchResult = JSON.parse(response);
        LM.searchResult([]);

        self.modelFiller(self.searchResult, LM.searchResult);
        if (!self.searchResult.length) {
            LM.emptySearch("Ничего не найдено!");
        }
    };

    this.getBestSchoolsCallback = function (response) {
        self.schools = self.shuffle(response, 3);
        //self.modelFiller(self.schools, LM.schools);
        LM.schools($.grep(self.schools, function (it) { return it.visible === "1"; }));
        self.size("#schools .b-course");
    };

    this.getBestCoursesCallback = function (response) {
        self.courses = self.shuffle(response, 3);
        self.modelFiller(self.courses, LM.courses);
        self.size("#courses .b-course");
    };

    this.getReviewsCallback = function (response) {
        self.reviews = self.shuffle(response, 2);

        ko.utils.arrayForEach(self.reviews, function (item) {
            item.complete = ko.computed(function () {
                return parseFloat((+item.study + +item.infra + +item.activ + +item.cost_q) / 4).toFixed(1);
            }, LM);
        });

        self.modelFiller(self.reviews, LM.reviews);
    };

    this.getBlogCallback = function (response) {
        self.blog = JSON.parse(response);

        self.modelFiller(self.blog, LM.blog);
    };

    this.size = function (selector) {
        var heights = [];
        $(selector).each(function(i, item) {
            heights.push($(item).height());
        });

        $(selector).height(Math.max.apply(null, heights));
    };

    this.shuffle = function(arr, len) {
        return _.shuffle(JSON.parse(arr)).slice(0, len);
    };

    this.init = function () {
        self.dataGot = 0;
        self.getBestSchools();
        self.getBestCourses();
        self.getReviews();
        self.getBlog();
    };
};



/**
 * @name {LayoutModel}
 * @return {undefined}
 */
var LayoutModel = function () {
    var self = this;

    this.schools = ko.observableArray([]);

    this.courses = ko.observableArray([]);

    this.reviews = ko.observableArray([]);

    this.blog = ko.observableArray([]);

    this.searchResult = ko.observableArray([]);

    this.age = ko.observable();
    this.language = ko.observable();
    this.type = ko.observable();

    this.emptySearch = ko.observable();
    
    this.search = function() {
        LC.search(self.age(), self.language(), self.type());
    };

};

var LM = new LayoutModel();

ko.applyBindings(LM);
