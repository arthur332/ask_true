﻿/**
 * @name {FilterController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var FilterController = function (controller, sourceType) {
    var self = this;

    this.dataGot = 0;

    this.dataNeed = 2;

    this.methods = {
        nzd: "https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22NZDRUB%22&format=json&env=store://datatables.org/alltableswithkeys&callback="
    };

    this.controller = controller;

    this.getData = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "list", type: sourceType } }, false)
            .done(function (response) { self.getDataCallback(response, "getData"); });
    };

    this.sort = function (sortType) {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "sort", type: sourceType, sortBy: sortType } }, false)
            .done(function (response) { self.getDataCallback(response, "sort"); });
    };

    this.filter = function (filterType, val){
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "filter", type: sourceType, filterBy: filterType, filterValue: val } }, false)
            .done(function (response) { self.getDataCallback(response, "filter"); });
    };

    this.filterMulty = function (filters) {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "filterMulty", type: sourceType, filters: filters } }, false)
            .done(function (response) { self.getDataCallback(response, "filter"); });
    }; 

    this.getSchools = function(id) {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "schools", id: id } }, false).done(self.getSchoolsCallback);
    };

    this.getNZD = function () {
        askApp.ajaxInterface({ url: self.methods.nzd }, false)
            .done(self.getNZDCallback)
            .fail(self.getNZDErrorCallback);
    };

    this.getNZDCallback = function(response) {
        self.nzd = response;

        //self.dataGot++;
        //askApp.dispatchEvent("dataGot");

        self.getCurrencies();
    };

    this.getNZDErrorCallback = function () {
        self.nzd = {};

        //self.dataGot++;
        //askApp.dispatchEvent("dataGot");

        self.getCurrencies();
    };

    this.getCurrencies = function () {
        askApp.ajaxInterface({ url: controller.concat(".php"), data: { get: "currencies", type: sourceType } }, false).done(self.getCurrenciesCallback);
    };

    this.getCurrenciesCallback = function (response) {

        self.currencies = self.mergeCurrencies(JSON.parse(response), self.nzd);


        self.cCurrencies = _.indexBy(self.currencies.Valute, "NumCode");
        self.dataGot++;
        askApp.dispatchEvent("dataGot");

        FM.currencyValue(self.cCurrencies[FM.currencyCode()]);

        self.getData();
    };

    this.mergeCurrencies = function(all, nzd) {
        all.Valute.push({
            CharCode: "NZD",
            Name: "Новозеландский доллар",
            Nominal: "1",
            NumCode: "554",
            //Value: "query" in nzd ? nzd.query.results.rate.Rate : "51.7355"
            Value: "51.7355"
        });

        return all;
    };

    this.getSchoolsCallback = function (response) {
        self.schools = $.grep(JSON.parse(response), function (it) { return it.visible === "1"; });
        FM.schools([]);

        ko.utils.arrayForEach(FC.schools, function (item, i) {
            (function (i) {
                item.cost = ko.computed(function () {
                    return !FM.currencyValue() ? item.costCalc : parseFloat(item.costCalc / FM.currencyValue()).toFixed(0);
                    //return parseFloat(item.costCalc * FM.currencyValue()).toFixed(0);
                }, FM);
            })(i);
        });

        /*for (var i in FC.schools) {
            if (FC.schools.hasOwnProperty(i)) {
                FM.schools.push(FC.schools[i]);
            }
        }*/
        FM.schools(self.schools);

        if (!self.schools.length) {
            FM.notFindBinders("Ничего не найдено!");
        } else {
            FM.notFindBinders("");
        }
    };

    this.serverResultBlock = $("#server-result");

    this.getDataCallback = function (response, callbackName) {
        
        self.data = JSON.parse(response);

        FM.listItems([]);

        try {
            ko.utils.arrayForEach(FC.data, function(item, i) {
                (function(i) {
                    item.cost = ko.computed(function() {
                        //return !FM.currencyValue() ? item.costCalc : parseFloat(item.costCalc / FM.currencyValue()).toFixed(0);

                        if (FM.currencyCode() == "643") {
                            if (FM.currencyCode() == item.costCurrency) {
                                return item.costCalc;
                            } else {
                                return parseFloat(item.costCalc * self.cCurrencies[item.costCurrency].Value.replace(",", ".")).toFixed(0);
                            }
                        } else {
                            if (item.costCurrency == "643") {
                                return parseFloat(item.costCalc / FM.currencyValue()).toFixed(0);
                            } else {
                                return parseFloat(item.costCalc * self.cCurrencies[item.costCurrency].Value.replace(",", ".") / FM.currencyValue()).toFixed(0);
                            }
                        }

                    }, FM);

                })(i);
            });
        } catch (e) {}

        /*for (var i in FC.data) {
            if (FC.data.hasOwnProperty(i)) {
                FM.listItems.push(FC.data[i]);
            }
        }*/

        if (callbackName === "getData" && (self.controller === "school" || self.controller === "course" || self.controller === "/countryPage")) {
            console.log("school:getData fiered deny");
        } else {
            self.serverResultBlock.remove();
            //FM.listItems((self.controller === "school" || self.controller === "/countryPage") ? $.grep(FC.data, function (it) { return it.visible === "1"; }) : FC.data);
            FM.listItems((self.controller === "school" || self.controller === "/countryPage") ? _.where(FC.data, { visible : "1" }) : FC.data);
        }

        self.dataGot ++;
        askApp.dispatchEvent("dataGot");
       
    };

    this.getParamsFilters = {
        country: "",
        language: "",
        city: "",
        spec:""
    };

    this.prepareURIComponent = function (component) {
        return decodeURIComponent(self.getParamsFilters[component]).replace(/"/g, "");
    };

    this.getGetParams = function () {

        self.getParamsFilters.language = askApp.serializeGetStringToObject(document.location.search, false, true).language || "";

        self.getParamsFilters.country = askApp.serializeGetStringToObject(document.location.search, false, true).country || "";

        self.getParamsFilters.city = askApp.serializeGetStringToObject(document.location.search, false, true).city || "";

        self.getParamsFilters.spec = askApp.serializeGetStringToObject(document.location.search, false, true).spec || "";
        

        FM.filterLangs(self.prepareURIComponent("language"));
        FM.filterCountry(self.prepareURIComponent("country"));
        FM.filterCity(self.prepareURIComponent("city"));
        FM.filterSpec(self.prepareURIComponent("spec"));

    };

    this.init = function() {
        self.dataGot = 0;
        self.getNZD();
        self.getGetParams();
    };
};



/**
 * @name {FilterModel}
 * @return {undefined}
 */
var FilterModel = function() {
    var self = this;

    this.listItems = ko.observableArray([]);

    this.filterByName = ko.observable(true);

    this.filterByCost = ko.observable(false);

    this.showCurrency = ko.observable(false);

    this.sort = function (data, event) {
        
        this.sortType = $(event.target || event.srcElement).data("sort");

        self.showCurrency(true);
        
        switch (this.sortType) {
            case "name":
                FC.sort("name");
                self.filterByName(true);
                self.filterByCost(false);
                break;
            case "cost":
                FC.sort("cost");
                self.filterByName(false);
                self.filterByCost(true);
                break;

            default: {
                FC.sort("name");
                self.filterByName(true);
                self.filterByCost(false);
                break;
            }
        }
    };

    this.filterLangs = ko.observable("");

    this.filterCountry = ko.observable("");

    this.filterCity = ko.observable("");

    this.filterSpec = ko.observable("");

    this.dropSort = function() {
        self.filterByName(true);
        self.filterByCost(false);
    };

    this.filterByLang = function() {
        self.dropSort();
        FC.filter("language", self.filterLangs);
    };

    this.filterByCity = function () {
        self.dropSort();
        FC.filter("city", self.filterCity);
    };

    this.filterByCountry = function() {
        self.dropSort();
        FC.filter("country", self.filterCountry);
    };

    this.filterBySpec = function () {
        self.dropSort();
        FC.filter("spec", self.filterSpec);
    };

    this.filterMulty = function () {
        self.dropSort();

        FC.filterMulty({
            language: self.filterLangs(),
            city: self.filterCity(),
            country: self.filterCountry(),
            spec: self.filterSpec()
        });
    };

    this.currencyCode = ko.observable("643");

    this.currencyValue = ko.observable("");
    
    

    this.currencyIconValue = ko.observable("Р");

    this.currencyIcon = function () {
        //console.log(self.currencyCode());
        switch (self.currencyCode()) {
            case 643:{
                self.currencyIconValue("Р");
                break;
            }
            case 840:{
                self.currencyIconValue("USD");
                break;
            }
            case 978: {
                self.currencyIconValue("EUR");
                break;
            }
            case 826: {
                self.currencyIconValue("GBP");
                break;
            }
            case 124: {
                self.currencyIconValue("CAD");
                break;
            }
            case "036": {
                self.currencyIconValue("AUD");
                break;
            }
            case 756: {
                self.currencyIconValue("CHF");
                break;
            } 
            case 554: {
                self.currencyIconValue("NZD");
                break;
            }
            case 156: {
                self.currencyIconValue("CNY");
                break;
            }
        }
    };

    this.changeCurrency = function(data, event) {
        var code = $(event.currentTarget || event.srcElement).data("code");
        if (code == "643") {
            self.currencyValue(false);
            
        }else{
            self.currencyValue($.grep(FC.currencies.Valute, function(item) {
                return item.NumCode == code;
            })[0].Value.replace(",","."));
        }

        self.currencyCode(code);

        self.currencyIcon();

        console.log(self.currencyValue());

    };

    this.currentCourseName = ko.observable("");

    this.schools = ko.observableArray([]);

    this.notFindBinders = ko.observable("");

    this.findSchools = function () {
        self.currentCourseName(this.name);
        FC.getSchools(this.id);
    };

    /*бронь из попапа joinedScools*/
    this.selectedSchoolReserve = ko.observable({name:""});
    this.reserveName = ko.observable("");
    this.reservePhone = ko.observable("");
    this.reserveEmail = ko.observable("");
    this.selectSchool = function () {
        $("#joinedSchools").modal("hide");
        self.selectedSchoolReserve(this);
    };

    this.reservePost = function () {
        var data = {
            course: self.currentCourseName(),
            school: self.selectedSchoolReserve().name,
            name: self.reserveName(),
            phone: self.reservePhone(),
            email: self.reserveEmail()
        };
        if (!!data.name.length && !!data.phone.length && !!data.email.length) {
            askApp.ajaxInterface({ url: FC.controller.concat(".php"), data: {
                get: 'reserve',
                course: data.course,
                school: data.school,
                name: data.name,
                phone: data.phone,
                email: data.email
            }
            }, false).done(self.reservePostCallback);
        } else {
            $.alert("Заполните все поля!");
        }
    };

    this.reservePostCallback = function (response) {
        self.reserveName("");
        self.reservePhone("");
        self.reserveEmail("");
        $.alert("Спасибо!", "success");
        $("#joinedSchoolsReserve").modal("hide");
    };

    /*бронь из попапа joinedScools end*/
};

var FM = new FilterModel();

$(document).on("dataGot", function () {
    if (FC.dataNeed === FC.dataGot) {
        ko.applyBindings(FM);

        if (JSON.stringify(FC.getParamsFilters).length > 48) {
            FM.filterMulty();
        }

        /*var a = FM.listItems();
        _.each(a, function(b) {
            b.description = "";
        });
        askApp.ajaxInterface({
            url: "/CourseHoliday",
            //data: { "jsonView": escape(ko.toJSON(FM.listItems())) }
            data: { "jsonView": ko.toJSON(a) }
            
        }, false).fail(function (r, e, m) { console.log(r, e, m) });*/
    }
});
