﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASKEducation.Models
{
    public class ImageFileModel
    {
        public string FileName { get; set; }
        public bool BigImageExist { get; set; }
    }
}