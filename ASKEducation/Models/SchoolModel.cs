﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASKEducation.Models
{
    public class SchoolModel
    {
        //public IList<string> FilePaths { get; set; }
        public string GalleryId { get; set; }
        public IEnumerable<ImageFileModel> Files { get; set; }

    }
}
