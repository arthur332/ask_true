﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASKEducation.Models;

namespace ASKEducation.Models
{
    public class CourseModel
    {
        public string id { get; set; }
        public string name { get; set; }
        //public string description { get; set; }
        public string type { get; set; }
        public string country { get; set; }
        public string language { get; set; }
        public string weeks { get; set; }
        public string cost { get; set; }
        public string costCalc { get; set; }
        public string best { get; set; }
        public string age { get; set; }
        public string studentsCount { get; set; }
        public string startCourse { get; set; }
        public string languageLevel { get; set; }
        public string costCurrency { get; set; }
        public string label { get; set; }

    }
}

public class CourseList
{
    public List<CourseModel> record;
}