﻿using System.Web;
using System.Web.Optimization;

namespace ASKEducation
{
    public class BundleConfig
    {
        // Дополнительные сведения о Bundling см. по адресу http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            /* bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-{version}.js"));
             **/
            bundles.Add(new ScriptBundle("~/bundles/ascjs").Include(
                        "~/Content/js/libs/niceScroll.js",
                        "~/Content/js/utils/utils.js",
                        "~/Content/js/libs/knockout-3.2.0.js",
                        "~/Content/js/libs/knockout.mapping-latest.js",
                        "~/Content/js/libs/format.js",
                        "~/Content/js/libs/underscore.js"));

            bundles.Add(new StyleBundle("~/bundles/askcss").Include(
                         "~/Content/css/common.css",
                         "~/Content/css/style.css",
                         "~/Content/css/font-awesome.css"));

             BundleTable.EnableOptimizations = true;
        }
    }
}