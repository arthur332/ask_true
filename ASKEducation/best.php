<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }
	
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	
	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	if($data["get"] == "currencies") {
		$currencies = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
		
		if(!$currencies) {
			$done = false;
			$currencies	= simplexml_load_file("valute.xml");
		}else{
			$done = true;
		}
		
		$currenciesJson = json_encode($currencies);
		echo($currenciesJson);
		
		if($done == true){
			file_put_contents('valute.xml', file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp"));
		}
	}
	
	if($data["get"] == "sort") {
		if($data['source'] == "courses"){
			$rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `best`= 1 ORDER BY ".$data['sortBy']." ASC");
		}else{
			$rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `popular`= 1 ORDER BY ".$data['sortBy']." ASC");
		}	
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "filter") {
		if($data['source'] == "courses"){
			$data["filterValue"] != "" 
			 ? $rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `best`= 1 AND ".$data['filterBy']." =? ORDER BY `name` ASC", $data["filterValue"])
			 : $rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `best`= 1 ORDER BY `name` ASC");
		}else{
			$data["filterValue"] != "" 
			 ? $rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `popular`= 1 AND ".$data['filterBy']." =? ORDER BY `name` ASC", $data["filterValue"])
			 : $rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `popular`= 1 ORDER BY `name` ASC");
		}
		
		echo json_encode(getFields($rs));
	}

    if($data["get"] == "filterMulty") {
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
        $filters = array();

        if($data["filters"]["language"] != ""){
            $filters[] = "language = '".$data["filters"]["language"]."'";
        }
        if($data["filters"]["city"] != ""){
            $filters[] = "city = '".$data["filters"]["city"]."'";
        }
        if($data["filters"]["country"] != ""){
            $filters[] = "country = '".$data["filters"]["country"]."'";
        }
        if($data["filters"]["spec"] != ""){
            $filters[] = "spec = '".$data["filters"]["spec"]."'";
        }

        if($data['source'] == "courses"){
            $prop = "best";
        }else{
            $prop = "popular";
        }

        if(count($filters)){
            $filtersSQL = implode(" AND ", $filters);

            $filtersSQL = " AND ".$filtersSQL;
        }else{
            $rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `".$prop."` = 1 ORDER BY `name` ASC");
            echo json_encode(getFields($rs));
            return;
        }

        if($data["filters"]["spec"] != ""){
            $rs = $DB->Execute("SELECT * FROM `linksspec` LEFT JOIN `schools` ON (`linksspec`.`schoolId`=`schools`.`id`) WHERE `popular` = 1 " .$filtersSQL);
        }else{
            $rs = $DB->Execute("SELECT * FROM `".$data['source']."` WHERE `".$prop."` = 1 " .$filtersSQL. " ORDER BY `name` ASC");
        }

        echo json_encode(getFields($rs));
    }

	if($data["get"] == "binded") {
		$rs = $DB->Execute("SELECT * FROM `links` LEFT JOIN `schools` ON (`links`.`schoolid`=`schools`.`id`) WHERE `courseid` =?", $data["id"]);
		
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "schools") {
        $rs = $DB->Execute("SELECT * FROM `schools` WHERE `popular` = 1");
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "courses") {
        $rs = $DB->Execute("SELECT * FROM `courses` WHERE `best` = 1");
		echo json_encode(getFields($rs));
	}
	
	/**Layout Requests*/
	
	
	if($data["get"] == "schoolsWhithCourses") {
        $res = $DB->Execute("select 
			s.id as s_id,
			s.city as s_city,
			s.costCalc as s_costCalc,
			s.country as s_country,
			s.description_long as s_description_long,
			s.image as s_image,
			s.language as s_language,
			s.name as s_name,
			s.popular as s_popular,
			s.visible as s_visible,
			s.schoolBirth as s_schoolBirth,
			s.schoolStartStudy as s_schoolStartStudy,
			s.studentAge as s_studentAge,
			s.studentAgeMiddle as s_studentAgeMiddle,
			s.studentsByClassRoom as s_studentsByClassRoom,
			s.type as s_type,
			c.id as c_id,
			c.age as c_age,
			c.best as c_best,
			c.cost as c_cost,
			c.costCalc as c_costCalc,
			c.country as c_country,
			c.description as c_description,
			c.language as c_language,
			c.languageLevel as c_languageLevel,
			c.name as c_name,
			c.startCourse as c_startCourse,
			c.studentsCount as c_studentsCount,
			c.type as c_type,
			c.weeks as c_weeks

			 from schools s 
			left join links l on l.schoolId=s.id
			left join courses c on c.id=l.courseId
			where s.popular=1 and s.id is not null
			order by s.id,c.id");
	
		$result = array();
		
							
		
		while(!$res->EOF) {
			$row = $res->fields;
			if (!isset($result[$row['s_id']])) {
				$result[$row['s_id']] = array(
						'id' => $row['s_id'],
						'city' => $row['s_city'],
						'costCalc' => $row['s_costCalc'],
						'country' => $row['s_country'],
						'description_long' => $row['s_description_long'],
						'image' => $row['s_image'],
						'language' => $row['s_language'],
						'name' => $row['s_name'],
						'popular' => $row['s_popular'],
						'visible' => $row['s_visible'],
						'schoolBirth' => $row['s_schoolBirth'],
						'schoolStartStudy' => $row['s_schoolStartStudy'],
						'studentAge' => $row['s_studentAge'],
						'studentAgeMiddle' => $row['s_studentAgeMiddle'],
						'studentsByClassRoom' => $row['s_studentsByClassRoom'],
						'type' => $row['s_type'],
						'children' => array()
				);
			}
			if (isset($result[$row['s_id']])) {
				$result[$row['s_id']]['children'][] = array(
					'id' => $row['c_id'],
					'age' => $row['c_age'],
					'best' => $row['c_best'],
					'cost' => $row['c_cost'],
					'costCalc' => $row['c_costCalc'],
					'country' => $row['c_country'],
					'description' => $row['c_description'],
					'language' => $row['c_language'],
					'languageLevel' => $row['c_languageLevel'],
					'name' => $row['c_name'],
					'startCourse' => $row['c_startCourse'],
					'studentsCount' => $row['c_studentsCount'],
					'type' => $row['c_type'],
					'weeks' => $row['c_weeks']
				);
			}
			$res->MoveNext();
		}
		echo json_encode($result);
		
	}
	
	
	if($data["get"] == "reviews") {
        $rs = $DB->Execute("SELECT * FROM `reviews` ORDER BY `name` ASC");
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "blog") {
        $rs = $DB->Execute("SELECT * FROM `blog` WHERE isBlog=1 ORDER BY `date_stamp` desc LIMIT 8");
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "coursesWhithSchools") {

        $res = $DB->Execute("select 
			s.id as s_id,
			s.city as s_city,
			s.costCalc as s_costCalc,
			s.country as s_country,
			s.description_long as s_description_long,
			s.image as s_image,
			s.language as s_language,
			s.name as s_name,
			s.popular as s_popular,
			s.visible as s_visible,
			s.schoolBirth as s_schoolBirth,
			s.schoolStartStudy as s_schoolStartStudy,
			s.studentAge as s_studentAge,
			s.studentAgeMiddle as s_studentAgeMiddle,
			s.studentsByClassRoom as s_studentsByClassRoom,
			s.type as s_type,
			c.id as c_id,
			c.age as c_age,
			c.best as c_best,
			c.cost as c_cost,
			c.costCalc as c_costCalc,
			c.label as c_label,
			c.country as c_country,
			c.description as c_description,
			c.language as c_language,
			c.languageLevel as c_languageLevel,
			c.name as c_name,
			c.startCourse as c_startCourse,
			c.studentsCount as c_studentsCount,
			c.type as c_type,
			c.weeks as c_weeks

			 from schools s 
			left join links l on l.schoolId=s.id
			left join courses c on c.id=l.courseId
			where c.best=1 and c.id is not null
			order by c.id,s.id");

		$result = array();
		
		while(!$res->EOF) {
			$row = $res->fields;

			if (!isset($result[$row['c_id']])) {
				$result[$row['c_id']] = array(
				'id' => $row['c_id'],
				'age' => $row['c_age'],
				'best' => $row['c_best'],
				'cost' => $row['c_cost'],
				'costCalc' => $row['c_costCalc'],
				'label' => $row['c_label'],
				'country' => $row['c_country'],
				'description' => $row['c_description'],
				'language' => $row['c_language'],
				'languageLevel' => $row['c_languageLevel'],
				'name' => $row['c_name'],
				'startCourse' => $row['c_startCourse'],
				'studentsCount' => $row['c_studentsCount'],
				'type' => $row['c_type'],
				'weeks' => $row['c_weeks'],
				'children' => array()
				);
			}

			if (isset($result[$row['c_id']])) {
				$result[$row['c_id']]['children'][] = array(
					'id' => $row['s_id'],
						'city' => $row['s_city'],
					    'costCalc' => $row['s_costCalc'],
						'country' => $row['s_country'],
						'description_long' => $row['s_description_long'],
						'image' => $row['s_image'],
						'language' => $row['s_language'],
						'name' => $row['s_name'],
						'popular' => $row['s_popular'],
						'visible' => $row['s_visible'],
						'schoolBirth' => $row['s_schoolBirth'],
						'schoolStartStudy' => $row['s_schoolStartStudy'],
						'studentAge' => $row['s_studentAge'],
						'studentAgeMiddle' => $row['s_studentAgeMiddle'],
						'studentsByClassRoom' => $row['s_studentsByClassRoom'],
						'type' => $row['s_type']
				);
				
				$res->MoveNext();
			}
		}


		echo json_encode($result);
	}
	
	if($data["get"] == "searchCourses") {
		
		if($data["age"] != "" && $data["type"] == "" && $data["language"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE age =? ORDER BY `name` ASC", $data["age"]));
			
		}
		
		if($data["language"] != "" && $data["type"] == "" && $data["age"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE language =? ORDER BY `name` ASC", $data["language"]));
		}
		
		if($data["type"] != "" && $data["language"] == "" && $data["age"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY `name` ASC", $data["type"]));
		}
		
		if($data["age"] != "" && $data["language"] != "" && $data["type"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE age =? AND  language=? ORDER BY `name` ASC", array($data["age"], $data["language"])));
		}
		
		if($data["age"] != "" && $data["type"] != "" && $data["language"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE age =? AND  type=? ORDER BY `name` ASC", array($data["age"], $data["type"])));
		}
		
		if($data["type"] != "" && $data["language"] != "" && $data["age"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE type =? AND  language=? ORDER BY `name` ASC", array($data["type"], $data["language"])));
		}
		
		if($data["type"] == "" && $data["language"] == "" && $data["age"] == ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` ORDER BY `name` ASC"));			
		}
		
		if($data["type"] != "" && $data["language"] != "" && $data["age"] != ""){
			$rs = getFields($DB->Execute("SELECT * FROM `courses` WHERE type =? AND  language=? AND  age=? ORDER BY `name` ASC", array($data["type"], $data["language"], $data["age"])));
		}
		
		if(count($rs) > 0) {
			foreach($rs as $k=>&$v){
				$v['schools'] =  getFields($DB->Execute("SELECT * FROM `links` LEFT JOIN `schools` ON (`links`.`schoolid`=`schools`.`id`) WHERE `courseid` =?", $v['id']));
			}
		}
		
		echo json_encode($rs);
	}
		
}

?>