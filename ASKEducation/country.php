<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	
	if($data["get"] == "list") {
		$rs = $DB->Execute("SELECT * FROM `countries` ORDER BY `name` ASC");
		echo json_encode(getFields($rs));
	}
	if($data["get"] == "country") {
		$rs = $DB->Execute("SELECT * FROM `countries` WHERE id=?", $data["id"]);
		echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "currencies") {
		$currencies = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
		$currenciesJson = json_encode($currencies);
		echo($currenciesJson);
	}
}

?>