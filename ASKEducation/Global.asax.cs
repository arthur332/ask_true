﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ASKEducation.Controllers;

namespace ASKEducation
{
    // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
    // см. по ссылке http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private static readonly Regex wwwRegex =
        new Regex(@"www\.(?<mainDomain>.*)",
                  RegexOptions.Compiled
                      | RegexOptions.IgnoreCase
                      | RegexOptions.Singleline);

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            // Do Not Allow URL to end in trailing slash
            string url = HttpContext.Current.Request.Url.AbsolutePath;
            if (!string.IsNullOrEmpty(url) && url.Length > 1)
            {
                string lastChar = url[url.Length - 1].ToString();
                if (lastChar == "/" || lastChar == "\\")
                {
                    url = url.Substring(0, url.Length - 1);
                    Response.Clear();
                    Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", url);
                    Response.End();
                    return;
                }
            }

            string hostName = Request.Headers["x-forwarded-host"];
            hostName = string.IsNullOrEmpty(hostName) ? Request.Url.Host : hostName;
            Match match = wwwRegex.Match(hostName);
            if (match.Success)
            {
                string mainDomain = match.Groups["mainDomain"].Value;
                var builder = new UriBuilder(Request.Url)
                {
                    Host = mainDomain
                };
                string redirectUrl = builder.Uri.ToString();
                Response.Clear();
                Response.StatusCode = 301;
                Response.StatusDescription = "Moved Permanently";
                Response.AddHeader("Location", redirectUrl);
                Response.End();
            }
        }

        /*protected void ApplicationResetSlash(Object sender, EventArgs e)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;

            string slash = url[url.Length - 1].ToString();

            if (slash == "/")
            {
                url = url.Substring(0, url.Length - 1);
                Response.Clear();
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", url);
                Response.End();
            }
        }*/

        protected void Application_Error()
        {
            var ex = Server.GetLastError();

            Response.StatusCode = 500;
            Response.Clear();
            Server.ClearError();

            var routeData = new RouteData();
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = "Index";
            routeData.Values["exception"] = ex;
            var httpEx = ex as HttpException;
            if (httpEx != null)
            {
                Response.StatusCode = httpEx.GetHttpCode();
                switch (Response.StatusCode)
                {
                    case 403:
                        routeData.Values["action"] = "Http403";
                        break;
                    case 404:
                        routeData.Values["action"] = "Http404";
                        break;
                }
            }
            Response.TrySkipIisCustomErrors = true;
            IController errorsController = new ErrorController();
            HttpContextWrapper wrapper = new HttpContextWrapper(Context);
            var rc = new RequestContext(wrapper, routeData);
            errorsController.Execute(rc);
        }


    }
}