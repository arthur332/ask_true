﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;

namespace ASKEducation.Controllers
{
    public class SchoolsHigherController : BaseController
    {
        //
        // GET: /SchoolsHigher/

        public ActionResult Index()
        {

            MySqlConnection Connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");
            //MySqlConnection Connection = new MySqlConnection("Database=ask_education;Data Source=127.0.0.1;User Id=root;Password=");



            string sSQL = @"SELECT * FROM countries";

            string city = @"SELECT * FROM city";

            DataTable dt = new DataTable();

            DataTable ct = new DataTable();

            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(sSQL, Connection);

                using (MySqlDataReader dr = com.ExecuteReader())
                {
                    //есть записи?
                    if (dr.HasRows)
                    {
                        try
                        {

                            dt.Load(dr);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }


            }

            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(city, Connection);

                using (MySqlDataReader ds = com.ExecuteReader())
                {
                    //есть записи?
                    if (ds.HasRows)
                    {
                        //заполняем объект DataTable
                        ct.Load(ds);
                    }
                }


            }

            string page = @"SELECT * FROM pages WHERE id = 7";
            DataTable pt = new DataTable();

            using (Connection)
            {
                Connection.Open();

                MySqlCommand command = new MySqlCommand(page, Connection);

                using (MySqlDataReader de = command.ExecuteReader())
                {
                    //есть записи?
                    if (de.HasRows)
                    {
                        //заполняем объект DataTable
                        try
                        {
                            pt.Load(de);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }


            }
            ViewBag.page = pt;



            ViewBag.countries = dt;
            ViewBag.city = ct;
            ViewBag.spec = GetSpec();

            ViewBag.schools = GetSchoolsBySchoolType("higher");

            return View();
        }

    }
}
