﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ASKEducation.Models;
using MySql.Data.MySqlClient;

namespace ASKEducation.Controllers
{
    public class SchoolController : BaseController
    {
        //
        // GET: /School/
        private const string ThumbDirs = @"\Content\images\school\galery\";
        private const string BigImageSubfolder = @"big\";

        public ActionResult GetImage(int id, string fileName, string mode)
        {
            if (mode == "thumb")
                return File(Path.Combine(ThumbDirs, id.ToString(), fileName), "image/jpeg");
            else
            {
                return File(Path.Combine(ThumbDirs, id.ToString(), BigImageSubfolder, fileName), "image/jpeg");
            }
        }

        public ActionResult Index(string id)
        {
            if (id == null )
            {
                return RedirectToAction("index", "Home");
            }

            var images = Directory.GetFiles(HttpContext.Server.MapPath(Path.Combine(ThumbDirs, id.ToString())))
                .Select(f => Path.GetFileName(f));

            var path = HttpContext.Server.MapPath(Path.Combine(ThumbDirs, id.ToString(), BigImageSubfolder));

            var bigImages = new List<string>();
            if (Directory.Exists(path))
                bigImages = Directory.GetFiles(path).Select(f => Path.GetFileName(f)).ToList();

            SchoolModel galleryModel = new SchoolModel()
            {
                Files = images.Select(f => new ImageFileModel(){ FileName =  f, BigImageExist = bigImages.Contains(f) }),
                GalleryId = id
            };

            MySqlConnection Connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");

            string sSQL = @"SELECT * FROM schools where id=" + id;

            
            DataTable dt = new DataTable();

            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(sSQL, Connection);

                using (MySqlDataReader dr = com.ExecuteReader())
                {
                    //есть записи?
                    if (dr.HasRows)
                    {
                        try
                        {
                            //заполняем объект DataTable
                            dt.Load(dr);
                        }
                        catch(Exception e) {}
                        
                    }
                }


            }

            ViewBag._school = dt;
            ViewBag._specs = GetSpecs(id);

            return View("Index", galleryModel);



        }

        public ActionResult Best()
        {
            MySqlConnection Connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");
            //MySqlConnection Connection = new MySqlConnection("Database=ask_education;Data Source=127.0.0.1;User Id=root;Password=");
            string sSQL = @"SELECT * FROM countries";

            string city = @"SELECT * FROM city";

            DataTable dt = new DataTable();

            DataTable ct = new DataTable();

            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(sSQL, Connection);

                using (MySqlDataReader dr = com.ExecuteReader())
                {
                    //есть записи?
                    if (dr.HasRows)
                    {
                        try
                        {

                            dt.Load(dr);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }


            }

            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(city, Connection);

                using (MySqlDataReader ds = com.ExecuteReader())
                {
                    //есть записи?
                    if (ds.HasRows)
                    {
                        //заполняем объект DataTable
                        ct.Load(ds);
                    }
                }


            }

            ViewBag.countries = dt;
            ViewBag.city = ct;
            ViewBag.spec = GetSpec();

            ViewBag.schools = GetSchoolsBySchoolType("best");

            return View();
        }

    }
}
