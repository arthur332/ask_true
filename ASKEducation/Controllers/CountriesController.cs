﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;

namespace ASKEducation.Controllers
{
    public class CountriesController : BaseController
    {
        //
        // GET: /Countries/

        public ActionResult Index()
        {

            ViewBag.countryTable = GetCountries("");

            return View();
        }

        public ActionResult Country()
        {
            string url = HttpContext.ApplicationInstance.Request.Url.AbsoluteUri;
            var newUrl = new Uri(url);
            var countryName = Uri.UnescapeDataString(newUrl.Segments[newUrl.Segments.Length - 1]);

            if (newUrl.LocalPath == "/Countries/country")
            {
                return RedirectToAction("index", "Countries");
            }


            MySqlConnection Connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&;charset=utf8");
                        
            const string city = @"SELECT * FROM city";

            
            DataTable ct = new DataTable();
            DataTable countryData = new DataTable();
                        
            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(city, Connection);

                using (MySqlDataReader ds = com.ExecuteReader())
                {
                    //есть записи?
                    if (ds.HasRows)
                    {
                         try
                        {
                        
                        ct.Load(ds);
                        }catch (Exception e) { }
                    }
                }

            }

            using (Connection)
            {
                Connection.Open();

                var c = new MySqlCommand("SELECT * FROM countries where name = @Value;", Connection);
                c.Parameters.AddWithValue("@Value", countryName);
                var dm = c.ExecuteReader();

                
                    //есть записи?
                    if (dm.HasRows)
                    {
                        try
                        {

                            countryData.Load(dm);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }

            }

            ViewBag.city = ct;
            ViewBag.countryData = countryData;

            ViewBag.countryList = GetData(@"SELECT * FROM schools WHERE visible=1 AND country = @Value ORDER BY `name` ASC", countryName);

            ViewBag.spec = GetSpec();

            return View();
        }

    }
}
