﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASKEducation.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index(Exception exception)
        {
            
            return View(exception);
        }

        public ActionResult Http403()
        {
            
            return View();
        }

        public ActionResult Http404()
        {
            
            return View();
        }

    }
}
