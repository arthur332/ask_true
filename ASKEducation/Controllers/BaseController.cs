﻿using System;
using System.Data;
using System.Web.Mvc;
using MySql.Data.MySqlClient;

namespace ASKEducation.Controllers
{
    public class BaseController:Controller
    {
        public DataTable GetSpec()
        {
            var connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");
            
            const string spec = @"SELECT * FROM spec";

            var specTable = new DataTable();


            using (connection)
            {
                connection.Open();

                var com = new MySqlCommand(spec, connection);

                using (var ds = com.ExecuteReader())
                {
                    if (ds.HasRows)
                    {
                        specTable.Load(ds);
                    }
                }


            }

            return specTable;
        }

        public DataTable GetSpecs(string id)
        {
            var connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");

            string spec = @"SELECT * FROM linksspec LEFT JOIN spec ON (linksspec.specId=spec.id) WHERE schoolId = " + id;

            var specTable = new DataTable();


            using (connection)
            {
                connection.Open();

                var com = new MySqlCommand(spec, connection);

                using (var ds = com.ExecuteReader())
                {
                    if (ds.HasRows)
                    {
                        try
                        {
                            specTable.Load(ds);
                        }
                        catch (Exception e)
                        {
                        }

                    }
                }


            }

            return specTable;
        }

        public DataTable GetSchoolsBySchoolType(string schoolType)
        {
            var connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");

            string schools;

            if (schoolType == "best")
            {
                schools = @"SELECT * FROM schools where visible=1 and popular = 1 order by name";
            }
            else
            {
                schools = @"SELECT * FROM schools where visible=1 and type = @Value;";
            }

            var schoolTable = new DataTable();


            using (connection)
            {
                connection.Open();

                var com = new MySqlCommand(schools, connection);
                
                if (schoolType != "best")
                {
                    com.Parameters.AddWithValue("@Value", schoolType);
                }

                using (var ds = com.ExecuteReader())
                {
                    if (ds.HasRows)
                    {
                        try {
                           schoolTable.Load(ds);
                        }
                        catch (Exception e){}

                    }
                }


            }

            return schoolTable;
        }

        public DataTable GetCountries(string countryName)
        {
            var connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&");

            string countries = countryName.Length > 0 ? @"SELECT * FROM countries name = @Value;" : @"SELECT * FROM countries";

            var countryTable = new DataTable();


            using (connection)
            {
                connection.Open();

                var com = new MySqlCommand(countries, connection);

                if (countryName.Length > 0)
                { 
                    com.Parameters.AddWithValue("@Value", countryName);
                }

                using (var ds = com.ExecuteReader())
                {
                    if (ds.HasRows)
                    {
                        try
                        {
                            countryTable.Load(ds);
                        }
                        catch (Exception e) { }

                    }
                }


            }

            return countryTable;
        }

        public DataTable GetData(string query, string param)
        {
            var connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&;charset=utf8");

            var dataTable = new DataTable();
            
            using (connection)
            {
                connection.Open();

                var com = new MySqlCommand(query, connection);

                if (param.Length > 0)
                {
                    com.Parameters.AddWithValue("@Value", param);
                }

                using (var dt = com.ExecuteReader())
                {
                    if (dt.HasRows)
                    {
                        try
                        {
                            dataTable.Load(dt);
                        }
                        catch (Exception e) { }

                    }
                }


            }

            return dataTable;
        }

    }
}