﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;

namespace ASKEducation.Controllers
{
    public class ProgramController : Controller
    {
        //
        // GET: /Program/

        public ActionResult Index()
        {

            MySqlConnection Connection = new MySqlConnection("Database=u0066974_ask_education;Data Source=37.140.192.132;User Id=u0066_root;Password=gEt7m29&;charset=utf8");
            const string countryDataRequestString = @"SELECT * FROM countries";
            DataTable countriesMenu = new DataTable();
            using (Connection)
            {
                Connection.Open();

                MySqlCommand com = new MySqlCommand(countryDataRequestString, Connection);

                using (MySqlDataReader menu = com.ExecuteReader())
                {
                    if (menu.HasRows)
                    {
                        try
                        {
                            countriesMenu.Load(menu);
                        }
                        catch (Exception e) { }
                    }
                }

            }

            ViewBag.countriesMenu = countriesMenu;

            return View();
        }

    }
}
