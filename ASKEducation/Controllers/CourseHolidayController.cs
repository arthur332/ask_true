﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ASKEducation.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace ASKEducation.Controllers
{
    public class CourseHolidayController : BaseController
    {
        //
        // GET: /CourseHoliday/

//        public ActionResult Index(List<CourseModel> jsonView)
//        {
//
//            ViewBag.page = GetData(@"SELECT * FROM pages WHERE id = 1", "");
//
//            ViewBag.countries = GetData(@"SELECT * FROM countries", "");
//
//            ViewBag.jsonView = null;
//
//            if (jsonView != null)
//            {
//                //var razorView = new JavaScriptSerializer().Deserialize<List<CourseList>>(jsonView);
//                ViewBag.jsonView = jsonView;
//
//                return View();
//            }
//
//            return View();
//        }
        public ActionResult Index(string jsonView)
        {

            ViewBag.page = GetData(@"SELECT * FROM pages WHERE id = 1", "");

            ViewBag.countries = GetData(@"SELECT * FROM countries", "");

            if (jsonView != null)
            {
                //ViewBag.jsonView = JsonConvert.DeserializeObject<CourseModel>(jsonView);

                var razorView = new JavaScriptSerializer().Deserialize<List<CourseModel>>(jsonView);
                
                ViewBag.jsonView = razorView;

                return View(razorView);
            }

            ViewBag.jsonView = null;

            ViewBag.courses = GetData(@"SELECT * FROM courses WHERE type = @Value ORDER BY `name` ASC", "holiday");

            return View();
        }

//        public IEnumerable<List<CourseModel>> ListItems(string jsonView)
//        {
//            var razorView = new JavaScriptSerializer().Deserialize<CourseModel>(jsonView);
//
//            return razorView;
//        }

    }
}
